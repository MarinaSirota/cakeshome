<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $price
 * @property int $category_id
 * @property int $user_id
 * @property float $rating
 */
class Product extends \yii\db\ActiveRecord
{
    const MAX_IMAGES = 5;

    public $photo;
    public $tag = [];
    public $new_tags;
    public $filter=[];


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'price', 'category_id', 'user_id'], 'required'],
            [['price', 'category_id', 'user_id'], 'integer'],
            [['name'], 'string', 'max' => 256],
            //[['description'], 'string', 'max' => 1024],
            [['photo'], 'file', 'maxFiles' => 10, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'price' => 'Цена',
            'category_id' => 'Category ID',
            'user_id' => 'User ID',
            'photo' => 'Фото',
            'tag' => 'Теги',
            'new_tags'=>'Новые теги',
            'filter'=>'Фильтры',
            'rating'=>'Рейтинг',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getImage()
    {
        return $this->hasOne(Image::className(), ['object_id' => 'id'])
            ->where(['type' => 2])
            ->where(['main' => 1]);
    }

    public function getImages()
    {
        return $this->hasMany(Image::className(), ['object_id' => 'id'])
            ->where(['type' => 2]);
    }

    public function getFilters()
    {
        return $this->hasMany(ProductFilter::className(), ['product_id' => 'id']);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['object_id' => 'id'])
            ->where(['type' => 2]);
    }

    public function getTags()
    {
        return $this->hasMany(ProductTag::className(), ['product_id' => 'id']);
    }

    public function checkedTagsArray()
    {
        $tags = [];
        foreach ($this->tags as $tag) {
            $tags[] = $tag->tag_id;
        }
        return $tags;
    }

    public function checkedFiltersArray()
    {
        $filters = [];
        foreach ($this->filters as $filter) {
            $filters[] = $filter->filter_id;
        }
        return $filters;
    }

    public function upload($image)
    {

        $fileName = implode('/',
                [
                    $this->id,
                    $image->id
                ]) . '.' .
            $this->photo->extension;
        $dir = implode('/',
            [
                $this->id,
            ]);

        if (!is_dir(Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $dir)) {
            mkdir(Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $dir, 0777, true);
        }
        $this->photo->saveAs(Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $fileName);
        if (!empty($fileName)) {
            $fileName = '/images/' . $fileName;



           /* $image_name='images/'.$fileName;
            if ($this->photo->extension=='jpg'){
                $image = imagecreatefromjpeg($image_name); // For JPEG

            }
            if ($this->photo->extension=='png'){
                $image = imagecreatefrompng($image_name);   // For PNG
            }
            $imgResized = imagescale($image , 100, 40); // width=500 and height = 400
            $resizeFile=imagejpeg($imgResized,$fileName);*/



            return $fileName;
        } else {
            return 'false';
        }
    }

    public function updateFilters()
    {
        /*var_dump(\Yii::$app->request->post());
        var_dump($this->filter);
        echo $this->filter;*/

        if($this->filter=='')
            ProductFilter::deleteAll(['product_id' => $this->id]);

        if($this->filter!=''){
            ProductFilter::deleteAll(['product_id' => $this->id]);
            foreach ($this->filter as $id) {
                $productFilter = new ProductFilter();
                $productFilter->filter_id = $id;
                $productFilter->product_id = $this->id;
                $productFilter->save();


            }
        }
    }

    public function updateTags()
    {
        if($this->tag=='')
            ProductTag::deleteAll(['product_id' => $this->id]);

        if($this->tag!=''){
            ProductTag::deleteAll(['product_id' => $this->id]);
            foreach ($this->tag as $id) {
                $productTag = new ProductTag();
                $productTag->tag_id = $id;
                $productTag->product_id = $this->id;
                $productTag->save();
            }

        }
        $this->addTags();
    }

    public function addTags()
    {
        $newTags=explode(',',$this->new_tags);
        foreach ($newTags as $newTagName) {
            $newTag = new Tag();
            $newTag->name = $newTagName;
            if($newTag->save())
            {
                $productTag = new ProductTag();
                $productTag->tag_id = $newTag->id;
                $productTag->product_id = $this->id;
                $productTag->save();
            }
        }
    }

    public function uploadImages()
    {
        $uploadFiles = UploadedFile::getInstancesByName('Product[photo]');
        $maxImages = self::MAX_IMAGES - count($this->images);
        if($maxImages > 0) {
            foreach ($uploadFiles as $photo) {
                $image = new Image();
                $image->object_id = $this->id;
                $image->type = 2;
                $image->main = ($maxImages === self::MAX_IMAGES) ? 1 : 0;
                $image->url = '';
                if ($image->save()) {
                    $this->photo = $photo;
                    $image->url = $this->upload($image);
                    $image->save();
                }
                if(--$maxImages == 0) {
                    break;
                }
            }
        }
    }

    public function afterDelete()
    {
        ProductTag::deleteAll(['product_id' => $this->id]);
        Image::deleteAll(['object_id' => $this->id, 'type'=>2]);
        Comment::deleteAll(['object_id' => $this->id, 'type'=>2]);
        Rating::deleteAll(['object_id' => $this->id, 'type'=>2]);

        parent::afterDelete(); // TODO: Change the autogenerated stub
    }

    public function updateRating()
    {
        $ratings=Rating::find()->where(['user_id'=>\Yii::$app->user->id,'object_id'=>$this->id, 'type'=>2])->all();
        $sum=0;
        $count=0;
        foreach ($ratings as $item){
            $count++;
            $sum=$sum+$item->value;
        }
        $this->rating=round($sum/$count,2);
        if(!$this->save()){
            print_r($this->errors);
            exit();
        }
    }

}
