<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "filter_category".
 *
 * @property int $id
 * @property string $name
 */
class FilterCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'filter_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    public function getFilters()
    {
        return $this->hasMany(Filter::className(), ['category_id' => 'id']);
    }
}
