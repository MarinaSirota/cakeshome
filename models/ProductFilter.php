<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_filter".
 *
 * @property int $product_id
 * @property int $filter_id
 * @property string $value
 */
class ProductFilter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_filter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'filter_id'], 'required'],
            [['product_id', 'filter_id'], 'integer'],
            [['value'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'filter_id' => 'Filter ID',
            'value' => 'Value',
        ];
    }



}
