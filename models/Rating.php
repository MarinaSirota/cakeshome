<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rating".
 *
 * @property int $user_id
 * @property int $object_id
 * @property int $type
 * @property int $value
 */
class Rating extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rating';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'object_id', 'type', 'value'], 'required'],
            [['user_id', 'object_id', 'type', 'value'], 'integer'],
            [['user_id', 'object_id'], 'unique', 'targetAttribute' => ['user_id', 'object_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'object_id' => 'Object ID',
            'type' => 'Type',
            'value' => 'Value',
        ];
    }
}
