<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "key_mail".
 *
 * @property int $user_id
 * @property string $value_key
 * @property int $used
 */
class KeyMail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'key_mail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'value_key'], 'required'],
            [['user_id', 'used'], 'integer'],
            //[['value_key'], 'string', 'max' => 256],

            [['user_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'value_key' => 'Value Key',
            'used' => 'Used',
        ];
    }
}
