<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property int $id
 * @property int $object_id
 * @property int $user_id
 * @property int $type
 * @property string $text
 * @property string $date
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['object_id', 'user_id', 'type', 'text', 'date'], 'required'],
            [['object_id', 'user_id', 'type'], 'integer'],
            [['date'], 'safe'],
            [['text'], 'string', 'max' => 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'object_id' => 'ID объекта',
            'user_id' => 'Автор',
            'type' => 'Тип',
            'text' => 'Текст',
            'date' => 'Дата',
        ];
    }
    public function getUser()
    {
        return $this->hasOne(User::className(),['id'=>'user_id']);
    }

}
