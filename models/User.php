<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $second_name
 * @property string $third_name
 * @property string $sity
 * @property string $address
 * @property string $description
 * @property string $phone
 * @property string $instagram
 * @property string $site
 * @property int $block
 * @property string $inn_number
 * @property string $user_type
 * @property string $email
 * @property string $password
 * @property string $authKey
 * @property int $active
 * @property float $rating
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const SCENARIO_USER='user';
    const SCENARIO_COOKER='cookers';
    const USER_TYPE_USER = 1;
    const USER_TYPE_COOKER = 2;
    public $okveds=[
        '10.71', '10.71.1','10.71.2','10.71.3',
        '10.72','10.72.1','10.72.3','10.72.4','10.72.31','10.72.32','10.72.35','10.72.39',
        '10.82','10.82.2','10.82.3','10.82.5','10.82.6',
        '46.36','46.36.1','46.36.2','46.36.3','46.36.4',
        '46.37',
        '47.24',
        '47.25.2',
        '47.29.35 ',
        '52.61',
        '52.61',
        '55.52',
        '47.81',
        '47.91',
        '47.99',
        '52.24',
        '56.10', //«Деятельность ресторанов и услуги по доставке продуктов питания».
    ];

    /*
        10.71 - Производство хлеба и мучных кондитерских изделий, тортов и пирожных недлительного хранения
        10.71.1 - Производство хлеба и хлебобулочных изделий недлительного хранения
        10.71.2 - Производство мучных кондитерских изделий, тортов и пирожных недлительного хранения
        10.71.3 - Производство охлажденных хлебобулочных полуфабрикатов
        10.72 - Производство сухарей, печенья и прочих сухарных хлебобулочных изделий, производство мучных кондитерских изделий, тортов, пирожных, пирогов и бисквитов, предназначенных для длительного хранения
        10.72.1 - Производство хрустящих хлебцев, сухарей и прочих сухарных хлебобулочных изделий
        10.72.3 - Производство прочих мучных кондитерских изделий длительного хранения
        10.72.31 - Производство печенья
        10.72.32 - Производство пряников и коврижек
        10.72.35 - Производство кексов, рулетов и аналогичных изделий длительного хранения
        10.72.39 - Производство восточных сладостей и прочих мучных кондитерских изделий
        10.82.2 - Производство шоколада и сахаристых кондитерских изделий
        10.82.3 - Производство кондитерских изделий из сахара
        10.82.5 - Производство засахаренных фруктов, орехов, цукатов из кожуры и прочих частей растений
        10.82.6 - Производство кондитерских леденцов и пастилок
        46.36 - Группа «Торговля оптовая сахаром, шоколадом и сахаристыми кондитерскими изделиями»
        46.36.1 - Торговля оптовая сахаром
        46.36.2 - Торговля оптовая шоколадом и сахаристыми кондитерскими изделиями
        46.36.3 - Торговля оптовая мучными кондитерскими изделиями
        46.36.4 - Торговля оптовая хлебобулочными изделиями
        46.37 - Торговля оптовая кофе, чаем, какао и пряностями
        47.24 - Торговля розничная хлебом и хлебобулочными изделиями и кондитерскими изделиями в специализированных магазинах
        47.25.2 - Торговля розничная безалкогольными напитками в специализированных магазинах
        47.29.35 - Торговля розничная чаем, кофе, какао в специализированных магазинах
        52.24- Розничная торговля хлебом, хлебобулочными и кондитерскими изделиями
     *  56.10 - «Деятельность ресторанов и услуги по доставке продуктов питания».
    */
    public $photo;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        /*foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }*/

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $user=User::findOne(['email'=>$username]);
        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return \Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }



    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'second_name', /*'third_name',*/ 'sity',  'phone', 'inn_number', 'user_type', 'email', 'password'], 'required','message' => 'Обязательное поле '],
            [['inn_number'], 'string'],
            //[['inn_number'],'unique', 'message' => 'Организация {value} уже зарегистрирована'],
            [['inn_number'], 'checkInn'],
            [['active'], 'integer'],
            [['user_type'], 'string'],
            [['first_name', 'second_name', 'third_name', 'sity', 'address', 'instagram', 'email', 'password'], 'string', 'max' => 256, 'message'=>'Не более 256 символов'],
            [['description'], 'string'],
            [['phone'], 'string', 'max' => 12],
            [['email'],'email'],
            [['email'],'unique', 'message' => 'Email {value} уже зарегистрирован'],
            //[['site'],'url'],
            [['password'],'string', 'min' => 6,'message' => 'Не менее 6 символов' ],
            [['first_name', 'second_name','third_name'],'match','pattern'=>'/[а-яА-я]+/us', 'message'=>'Только буквы русского алфавита.'],
            //[['phone'], 'match','pattern'=>'^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$']
            [['photo'], 'file', 'extensions' => 'png, jpg', 'message' => 'Формат jpg, png'],
        ];
    }

    public function checkInn($attribute, $params, $validator)
    {
        $api = new DadataApi();
        $result = $api->checkInn($this->$attribute);
        $suggestions= $result['suggestions'];

        if(count($result['suggestions'])===0) {
            $this->addError($attribute, 'Неверный ИНН');
        } else {
            if(count($result['suggestions'])!==0) {

                $s0=$suggestions['0'];
                $data=$s0['data'];
                $management=$data['management'];
                $state=$data['state'];
                $address=$data['address'];
                $dataInAddress=$address['data'];
                /*if($this->checkManagement($management)==false){
                    $this->addError($attribute, 'Неверное имя директора огранизации');
                }*/
                if($state['status']!='ACTIVE'){
                    $this->addError($attribute, 'Указанная организация неактивна');
                }
                if(!in_array($data['okved'], $this->okveds)){
                    $this->addError($attribute, 'Некорректный вид дейтельности');
                }
            }
        }

    }

    public function checkManagement($management)
    {
        /*$array=($this->second_name, $this->first_name,$this->third_name);
        $checking=implode(' ',$this->second_name, $this->first_name,$this->third_name);*/
        $checking=str_replace(' ', '', $this->second_name).' '.str_replace(' ', '', $this->first_name).' '.str_replace(' ', '', $this->third_name);
        if($management['name']==$checking){
            return true;
        }
        else{
            return false;
        }

    }
    public function checkRegion($dataInAddress)
    {
        /*$array=($this->second_name, $this->first_name,$this->third_name);
        $checking=implode(' ',$this->second_name, $this->first_name,$this->third_name);*/
        if($dataInAddress['region_kladr_id']==9200000000000 || $dataInAddress['region_kladr_id']==9100000000000){
            return true;
        }
        else{
            return false;
        }

    }

    public function checkOkved($okved)
    {
        if(in_array($okved, $this->okveds)){
            return true;
        }
        else{
            return false;
        }
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'Имя',
            'second_name' => 'Фамилия',
            'third_name' => 'Отчество',
            'sity' => 'Город',
            'address' => 'Адрес',
            'description' => 'Описание',
            'phone' => 'Телефон',
            'instagram' => 'Инстаграм',
            'site' => 'Сайт',
            'block' => 'Блок',
            'inn_number' => 'ИНН',
            'user_type' => 'User Type',
            'email' => 'Email',
            'password' => 'Password',
            'active'=>'Active',
            'photo'=>'Фото',
            'rating'=>'Рейтинг'
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_USER] = ['email' , 'password'];
        $scenarios[self::SCENARIO_COOKER] = ['email', 'password','first_name', 'second_name', 'third_name', 'sity', 'phone', 'inn_number',];
        return $scenarios;
    }
    public function beforeSave($insert)
    {
        if($insert)
        {
            $this->password = \Yii::$app->getSecurity()->generatePasswordHash($this->password);
            $this->authKey = \Yii::$app->security->generateRandomString();
            switch ($this->scenario) {
                case self::SCENARIO_COOKER:
                    $this->user_type = self::USER_TYPE_COOKER;
                    break;
                case self::SCENARIO_USER:
                    $this->user_type = self::USER_TYPE_USER;
                    break;
            }
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }




    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['product_id' => 'id']);
    }

    public function getImage()
    {
        return $this->hasOne(Image::className(), ['object_id' => 'id'])
            ->where(['type'=>1]);
    }

    public function getComments()
    {
        return $this->hasMany(Comment::className(),['object_id'=>'id'])
            ->where(['type'=>1]);
    }

    public function getTasks()
    {
        if($this->user_type==1) {
            return $this->hasMany(Task::className(), ['user_id' => 'id']);
        }

        if($this->user_type==2) {
            return $this->hasMany(Task::className(), ['cooker_id' => 'id'])->orderBy(['date_added'=>SORT_DESC]);
        }
    }

    public function upload()
    {

        $fileName = '/users/'. $this->id.'/'. $this->id. '.' . $this->photo->extension;
        $dir = '/users/'. $this->id;

        if (!is_dir(Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $dir)) {
            mkdir(Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $dir, 0777, true);
        }
        $this->photo->saveAs(Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $fileName);
        if (!empty($fileName)) {
            $fileName = '/images' . $fileName;
            return $fileName;
        } else {
            return 'false';
        }
    }

    public function uploadImage()
    {
        $uploadFile = UploadedFile::getInstance($this,'photo');
        $this->photo = $uploadFile;
        //echo $this->photo;
        //exit();
        if($uploadFile) {
            Image::deleteAll(['object_id' => $this->id, 'type' => 1]);

            $image = new Image();
            $image->object_id = $this->id;
            $image->type = 1;
            $image->main = 1;
            $image->url = $this->upload();
            $image->save();
        }
    }

    public function defaultImage(){
        $image = new Image();
        $image->object_id = $this->id;
        $image->type = 1;
        $image->main = 1;
        $image->url ='/images/userProfile.png' ;
        $image->save();
    }

    public function sendCode()
    {

        $key = new KeyMail();
        $key->user_id = $this->id;
        srand($this->make_seed());
        $key->value_key= rand(1000, 9999);


        if($key->save()){

            try {
                $title='Подтверждение аккаунта';

                $boolResult = \Yii::$app->mailer->compose('user/confirm', [
                        'code' => $key->value_key,
                    ]
                )
                    ->setFrom(['noreply@cakeshome.com' => 'Cakes Home'])
                    ->setTo($this->email)
                    ->setSubject($title)
                    ->send();
            } catch (\Swift_TransportException $e) {
                return false;
            } catch (MissingRequiredParameters $e) {
                return false;
            }
        }
    }

    public function make_seed()
    {
        list($usec, $sec) = explode(' ', microtime());
        return $sec + $usec * 1000000;
    }

    public function updateRating()
    {
        $ratings=Rating::find()->where(['user_id'=>\Yii::$app->user->id,'object_id'=>$this->id, 'type'=>1])->all();
        $sum=0;
        $count=0;
        foreach ($ratings as $item){
            $count++;
            $sum=$sum+$item->value;
        }
        $this->rating=round($sum/$count,2);
        if(!$this->save()){
            print_r($this->errors);
            exit();
        }
    }


}
