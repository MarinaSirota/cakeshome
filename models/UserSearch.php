<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form of `app\models\User`.
 */
class UserSearch extends User
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'block', 'inn_number'], 'integer'],
            [['first_name', 'second_name', 'third_name', 'sity', 'address', 'description', 'phone', 'instagram', 'site', 'user_type', 'email', 'password', 'authKey'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $cooker_search_text=\Yii::$app->request->get('cooker_search_text', \Yii::$app->request->post('cooker_search_text'));
        if(is_null($cooker_search_text)) {
            $query = User::find();
        } else {
            $query=User::findBySql('
                SELECT * from user WHERE  user_type=2 and (first_name LIKE :cooker_search_text OR  second_name LIKE :cooker_search_text
                OR  sity LIKE :cooker_search_text)
                ',[':cooker_search_text'=>"%$cooker_search_text%"]);
        }

        // add conditions that should always apply here
        $query->orderBy(['rating'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'block' => $this->block,
            'inn_number' => $this->inn_number,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'second_name', $this->second_name])
            ->andFilterWhere(['like', 'third_name', $this->third_name])
            ->andFilterWhere(['like', 'sity', $this->sity])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'instagram', $this->instagram])
            ->andFilterWhere(['like', 'site', $this->site])
            ->andFilterWhere(['like', 'user_type', $this->user_type])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'authKey', $this->authKey]);

        return $dataProvider;
    }
}
