<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $phone
 * @property string $sity
 * @property string $date_added
 * @property int $category_id
 * @property string $description
 * @property string|null $date
 * @property int|null $cooker_id
 * @property int $state
 * @property int|null $min_price
 * @property int|null $max_price
 * @property int|null $delivery
 * @property string $email
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'sity', 'date_added', 'category_id', 'description', 'state', 'email'], 'required'],
            [['user_id', 'category_id', 'cooker_id', 'state', 'min_price', 'max_price', 'delivery'], 'integer'],
            [['date_added'], 'safe'],
            [['description'], 'string'],
            [['phone'], 'string', 'max' => 12],
            [['sity', 'date', 'email'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'phone' => 'Телефон',
            'sity' => 'Город',
            'date_added' => 'Дата',
            'category_id' => 'Категория',
            'description' => 'Описание',
            'date' => 'Срок',
            'cooker_id' => 'Cooker ID',
            'state' => 'Статус',
            'min_price' => 'Цена От',
            'max_price' => 'Цена До',
            'delivery' => 'Способ доставки',
            'email' => 'Email',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }


    public function sendToUserEmail()
    {
        try {
            $title='Ваша заявка принята';

            $boolResult = \Yii::$app->mailer->compose('user/task', [
                    'email' => \Yii::$app->user->getIdentity()->email,
                    'phone' => \Yii::$app->user->getIdentity()->phone,
                    'task'=>$this
                ]
            )
                ->setFrom(['noreply@cakeshome.com' => 'Cakes Home'])
                ->setTo($this->email)
                ->setSubject($title)
                ->send();
        } catch (\Swift_TransportException $e) {
            return false;
        } catch (MissingRequiredParameters $e) {
            return false;
        }

    }

    public function sendToCookerEmail($product)
    {
        try {
            $title='У вас новый заказ!';

            $boolResult = \Yii::$app->mailer->compose('user/order', [
                    'order'=>$this,
                    'product'=>$product
                ]
            )
                ->setFrom(['noreply@cakeshome.com' => 'Cakes Home'])
                ->setTo($product->user->email)
                ->setSubject($title)
                ->send();
        } catch (\Swift_TransportException $e) {
            return false;
        } catch (MissingRequiredParameters $e) {
            return false;
        }

    }

}
