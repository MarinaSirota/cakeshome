<?php

namespace app\models;

use app\models\Product;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * ProductSearch represents the model behind the search form of `app\models\Product`.
 */
class ProductSearch extends Product
{

    public $filters = [];
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'price', 'category_id', 'user_id'], 'integer'],
            [['name', 'description'], 'safe'],
            [['filters'], 'default'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $searchText=\Yii::$app->request->get('search_text', \Yii::$app->request->post('search_text'));
        $searchTextEnglish=\Yii::$app->request->get('search_text_english', \Yii::$app->request->post('search_text_english'));

        if (preg_match("/^[a-z ]+$/i", $searchText)) {
            $searchText = mb_strtolower($searchText, 'UTF-8');
            $searchText=$this->euToRu($searchText);
        }

        if(is_null($searchText)) {
            $query = Product::find();
        } else {
            if(!is_null($searchText)) {
                $query = Product::findBySql('
                SELECT * from product WHERE name LIKE :searchText OR id in (
                    SELECT product_id from product_tag WHERE 
                    tag_id in (
                        SELECT id from tag WHERE name LIKE :searchText
                        )
                    )', [':searchText' => "%$searchText%"]);
            }
            else {
                if(!is_null($searchTextEnglish)){
                    $query=Product::findBySql('
                SELECT * from product WHERE name LIKE :searchText OR id in (
                    SELECT product_id from product_tag WHERE 
                    tag_id in (
                        SELECT id from tag WHERE name LIKE :searchText
                        )
                    )',[':searchText'=>"%$searchTextEnglish%"]);
                }
            }
        }


        // add conditions that should always apply here

        if(!empty($params['filters'])) {

            foreach ($params['filters'] as $filterId) {
                $query->andWhere(['id' =>
                    (new Query())->select('product_id')->from('product_filter')->where(['filter_id' => $filterId])
                ]);

            }
            /*            $query->andWhere(['id' =>
                            (new Query())->select('product_id')->from('product_filter')->where(['filter_id' => $params['filters']])
                        ]);*/
        }
        if(!empty($params['category'])) {
            $query->andWhere(['category_id' => $params['category']]);
        }

        if(!empty($params['min_price'])) {
            $query->andWhere(['>=', 'price', $params['min_price']]);
        }
        if(!empty($params['max_price'])) {
            $query->andWhere(['<=', 'price', $params['max_price']]);
        }
        if(!empty($params['sort'])) {
            if($params['sort']=='price_desc') {
                $query->orderBy(['price'=>SORT_DESC]);
            }
            if($params['sort']=='price_asc') {
                $query->orderBy(['price'=>SORT_ASC]);
            }
            if($params['sort']=='name_desc') {
                $query->orderBy(['name'=>SORT_DESC]);
            }
            if($params['sort']=='name_asc') {
                $query->orderBy(['name'=>SORT_ASC]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'category_id' => $this->category_id,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }

    public function ruToEu ($text)
    {
        $str_search = array(
            "й","ц","у","к","е","н","г","ш","щ","з","х","ъ",
            "ф","ы","в","а","п","р","о","л","д","ж","э",
            "я","ч","с","м","и","т","ь","б","ю"
        );
        $str_replace = array(
            "q","w","e","r","t","y","u","i","o","p","[","]",
            "a","s","d","f","g","h","j","k","l",";","'",
            "z","x","c","v","b","n","m",",","."
        );
        return str_replace($str_search, $str_replace, $text);
    }
    public function euToRu ($text)
    {
        $str_search = array(
            "q","w","e","r","t","y","u","i","o","p","[","]",
            "a","s","d","f","g","h","j","k","l",";","'",
            "z","x","c","v","b","n","m",",","."
        );
        $str_replace = array(
            "й","ц","у","к","е","н","г","ш","щ","з","х","ъ",
            "ф","ы","в","а","п","р","о","л","д","ж","э",
            "я","ч","с","м","и","т","ь","б","ю"
        );
        return str_replace($str_search, $str_replace, $text);
    }
}
