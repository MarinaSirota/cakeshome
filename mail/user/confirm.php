
<div class="message">
    <h1>Добро пожаловать на Cakes Home!</h1>
    <h2>Для подтверждения аккаунта введите код </h2>
    <button class="button"> КОД <?php
        print $code;
        ?> </button>
    <h4>С Уважением, Cakes Home!</h4>
</div>

<style>

    .button{
        background-color: rgba(205, 169, 172, 0.33);
        text-align: center;
        width: 300px;
        height: 50px;
        outline: 0;
        border: 0;
        border-radius: 4px;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: #000000;
        cursor: pointer;
    }
</style>
