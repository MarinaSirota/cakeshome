
<div class="message">
    <h1>На связи Cakes Home!</h1>
    <h2>Ваш заказ взят</h2>
    <p> <?=$task->description?></p>
    <button class="button">
        Email: <?=$email?>
        Телефон: <?=$phone?>
    </button>
    <h4>С Уважением, Cakes Home!</h4>
</div>

<style>

    .button{
        background-color: rgba(205, 169, 172, 0.64);
        text-align: center;
        width: 300px;
        height: 50px;
        outline: 0;
        border: 0;
        border-radius: 4px;
        text-transform: uppercase;
        letter-spacing: 1px;
        color: #f5f5f5;
        cursor: pointer;
    }
</style>
