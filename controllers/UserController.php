<?php

namespace app\controllers;

use app\models\Category;
use app\models\FilterCategory;
use app\models\Image;
use app\models\Product;
use app\models\ProductSearch;
use app\models\User;
use app\models\UserSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;


class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
       //$scenario = \Yii::$app->request->get('type', \Yii::$app->request->post('type', 'user'));
        //$model=new User(['scenario' => $scenario]);
       // $searchModel = new UserSearch(['user_type'=>'2']);
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $sellers = User::find()->where(['user_type'=>'1'])->all();
        //$filtersCategory=FilterCategory::find()->all();
        //$image=Image::find()->where(['type'=>2, 'main'=>1])->all();

        return $this->render('index', [
            //'searchModel' => $searchModel,
           // 'dataProvider' => $dataProvider,
            //'categories'=> $categories,
            //'filtersCategory'=>$filtersCategory
            'sellers'=>$sellers

        ]);

    }


    public function actionProfile($id)
    {
        //$scenario = \Yii::$app->request->get('type', \Yii::$app->request->post('type', 'user'));
        $model=User::findOne($id);
        if($model->user_type==2){
            return $this->redirect("/seller/cooker/$id");
        }

        return $this->render('profile',
            [
                'model'=>$model
            ]);
    }
    public function actionUpdate()
    {
        $id=\Yii::$app->user->identity->id;
        $model = $this->findModel($id);


        if (\Yii::$app->request->post()) {
            $model->first_name=\Yii::$app->request->post('User')['first_name'];
            $model->photo=\Yii::$app->request->post('User')['photo'];

            $model->uploadImage();
            $model->save(false);
            return $this->redirect("/user/profile/$id");
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


}
