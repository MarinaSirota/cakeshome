<?php

namespace app\controllers;

use app\models\Category;
use app\models\DadataApi;
use app\models\KeyMail;
use app\models\Product;
use app\models\ProductSearch;
use app\models\User;
use app\models\UserForm;
use Yii;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $products=Product::find()->orderBy('id')->offset('1')->limit('4')->all();
        $categories = Category::find()->all();
        return $this->render('index',[
            'products'=>$products,
            'categories'=>$categories
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
          /*  if (Yii::$app->user->identity->user_type==='2')
                return $this->render('/cooker');
            if (Yii::$app->user->identity->user_type==='3')
                return $this->render('/admin');*/
            return $this->goHome();
        }

        $model = new UserForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            if ($model->getType()==2)
            {
                return $this->redirect('/cooker');
            }
            return $this->goBack();
        }
        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    public function actionCode()
    {
        return $this->render('code');
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionRegistration()
    {

        $scenario = \Yii::$app->request->post('type', 'user');

        $model=new User(['scenario' => $scenario]);

        if (!\Yii::$app->request->isPost)
        {
            return  $this->render('registration', [
                    'model'=>$model
            ]);
        }
        if($model->load(\Yii::$app->request->post())){
            //print_r(\Yii::$app->request->post());
            if($model->validate()) {
                $users = User::find()->where(['email' => $model->email])->all(); // если этой проверки нет , то при повторной
                // отправке формы
                // user записывается опять

                if(count($users) > 0) {
                    return $this->render('index', [

                    ]);
                }
                /*print_r($model);
                exit();*/
                if($model->save()) {

                    $model->defaultImage();
                    $model->sendCode();

                    return $this->render('code', [

                    ]);
                }

            } else {
                return  $this->render('registration', [
                    'model'=>$model
                ]);
            }
        }

    }



    public function actionCheckKey()
    {

        //if(Yii::$app->request->post('val')){
            $check=Yii::$app->request->post('code');
            $userKey = KeyMail::findOne(['value_key' => $check=Yii::$app->request->post('code')]);
            if(is_object($userKey)) {
                $user = User::findOne(['id' => $userKey->user_id]);
                $user->scenario = User::SCENARIO_USER;
                if(is_object($user)) {
                    $userKey->used = 1;
                    $user->active = 1;
                    if(!$user->save()){
                        print_r($user->getErrors()); exit();
                    }
                    $userKey->save();
                    return $this->redirect('login');

                }
            }else {
                $error='Неверный код';
                return $this->render('code', [
                    'error'=>$error

                ]);
            }

    }

    public function actionAgree()
    {
        return $this->render('agree');
    }

    public function actionTest()
    {
        $okveds=['10.71','52.61',' 55.52',' 47.81', '47.91', '47.99'];
        $api = new DadataApi();
        $result=$api->checkInn('9102171153');
        print_r($result);

        $suggestions= $result['suggestions'];
        $s0=$suggestions['0'];
        $data=$s0['data'];

        $management=$data['management'];
        $state=$data['state'];
        $address=$data['address'];
        $dataInAddress=$address['data'];

        //print_r($state);
       /* echo $dataInAddress['region_kladr_id'];*/


    /*    if($dataInAddress['region_kladr_id']==9200000000000 || $dataInAddress['region_kladr_id']==9100000000000){
            echo 'РЕГИОН';
        }*/

       /* if($management['name']=='Володина Надежда Яковлевна'){
            echo $management['name'];
        }*/
        var_dump( $management);

      /*  if($state['status']=='ACTIVE'){
            echo $state['status'];
        }

        if(in_array($data['okved'], $okveds)){
            echo 'ОКВЭД';
        }*/


    }

}
