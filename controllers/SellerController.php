<?php

namespace app\controllers;

use app\models\Category;
use app\models\Comment;
use app\models\FilterCategory;
use app\models\Image;
use app\models\Product;
use app\models\ProductSearch;
use app\models\Rating;
use app\models\User;
use app\models\UserSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;


class SellerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $searchModel = new UserSearch(['user_type'=>'2']);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [

            'dataProvider'=>$dataProvider

        ]);

    }


    public function actionCooker( $id)
    {
        $cooker=User::findOne($id);
        $productsSearchModel=new ProductSearch(['user_id'=>$id]);
        $products=$productsSearchModel->search(Yii::$app->request->queryParams);
        $pr_ids=[];
        foreach ($products->getModels() as $product){
            $pr_ids[]=$product->id;
        }
        //var_dump($pr_ids);

        return $this->render('cooker', [
            'cooker' => $cooker,
            'products'=>$products,
            'id'=>$id,
            'pr_ids'=> $pr_ids
        ]);

    }

    public function actionSearch()
    {
        $cooker_search_text=Yii::$app->request->get('cooker_search_text', \Yii::$app->request->post('cooker_search_text'));
        $searchModel = new UserSearch(['first_name'=>$cooker_search_text]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // print_r($dataProvider->createCommand()->sql);

        if($dataProvider->getCount()==0)
        {
            return $this->render('index');
        }
        else {
            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionAddComment(){
        Yii::$app->response->format = 'json';

        $user_name=Yii::$app->user->getIdentity()->first_name;
        if(Yii::$app->request->post('id')){
            $comment=new Comment();
            $comment->object_id=Yii::$app->request->post('id');
            $comment->type=1;
            $comment->user_id=Yii::$app->user->id;
            // $comment->user_id=Yii::$app->request->post('user_id');
            $comment->text=Yii::$app->request->post('text');
            $comment->date=date("Y-m-d H:i:s");
            $comment->save();
            if($comment->save()){
                $data=[
                    'success'=>true,
                    'date'=>$comment->date,
                    'id'=>$comment->id,
                    'user'=>$comment->user_id,
                    'name'=>$user_name
                ];

            }else{
                $data=['success'=>$comment->errors];
            }
        }
        return $data;
    }

    public function actionDeleteComment(){
        Yii::$app->response->format = 'json';

        if(Yii::$app->request->post('id')){
            $comment=Comment::findOne(Yii::$app->request->post('id'))->delete();

            $data=['success'=>true,

            ];
        }else{
            $data=['success'=>false];
        }

        return $data;
    }

    public function actionRating(){
        Yii::$app->response->format = 'json';
        Rating::deleteAll(['user_id'=>\Yii::$app->user->id,'object_id'=>\Yii::$app->request->post('object_id'), 'type'=>1]);
        $rating=new Rating();
        $rating->user_id=\Yii::$app->user->id;
        $rating->object_id=\Yii::$app->request->post('object_id');
        $rating->type=1;
        $rating->value=\Yii::$app->request->post('mark');
        if($rating->save()){
            $cooker=User::findOne(['id'=>\Yii::$app->request->post('object_id')]);
            $cooker->updateRating();

            $data=['success'=>true,
                'rating'=>round($cooker->rating)
            ];

        }
        else{
            $data=['success'=>false];
        }

        return $data;
    }



}
