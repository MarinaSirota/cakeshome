<?php

namespace app\controllers;

use app\models\Category;
use app\models\Comment;
use app\models\CommentSearch;
use app\models\Filter;
use app\models\FilterCategory;
use app\models\Product;
use app\models\ProductFilter;
use app\models\ProductSearch;
use app\models\Rating;
use app\models\Tag;
use app\models\TagSearch;
use app\models\Task;
use app\models\User;
use stdClass;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;


class CatalogController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $categories = Category::find()->orderBy('name')->all();
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $filtersCategory=FilterCategory::find()->all();
        $max=\Yii::$app->db->createCommand('SELECT MAX(price) as a FROM product')->queryScalar();
        $min=\Yii::$app->db->createCommand('SELECT MIN(price) as i FROM product')->queryScalar();
        if($dataProvider->getCount()==0)
        {
            return $this->render('index');
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'categories'=> $categories,
            'filtersCategory'=>$filtersCategory,
            'max'=>(int)$max,
            'min'=>(int)$min,
            'params' => Yii::$app->request->queryParams,

        ]);

    }
    public function actionCategory($id)
    {
        $categories =Category::find()->orderBy('name')->all();
        $searchModel = new ProductSearch(['category_id'=>$id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $filtersCategory=FilterCategory::find()->all();
        $max=\Yii::$app->db->createCommand('SELECT MAX(price) as a FROM product')->queryScalar();
        $min=\Yii::$app->db->createCommand('SELECT MIN(price) as i FROM product')->queryScalar();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categories'=> $categories,
            'filtersCategory'=>$filtersCategory,
            'max'=>(int)$max,
            'min'=>(int)$min
        ]);

    }
    public function actionProduct( $id)
    {
        $searchModel = new ProductSearch(['id'=>$id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('product', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id'=>$id,

        ]);

    }

    public function actionSearch()
    {
        $searchText=Yii::$app->request->get('search_text', \Yii::$app->request->post('search_text'));
        $searchTextEnglish=Yii::$app->request->get('search_text_english', \Yii::$app->request->post('search_text_english'));
        if(!is_null($searchText)){
            $searchModel = new ProductSearch(['name'=>$searchText]);
        } else{
            if(!is_null($searchTextEnglish)){
                $searchModel = new ProductSearch(['name'=>$searchTextEnglish]);
            }
        }
        //$searchModel = new ProductSearch(['name'=>$searchText]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // print_r($dataProvider->createCommand()->sql);


        $filtersCategory=FilterCategory::find()->all();
        $categories =Category::find()->orderBy('name')->all();
        $max=\Yii::$app->db->createCommand('SELECT MAX(price) as a FROM product')->queryScalar();
        $min=\Yii::$app->db->createCommand('SELECT MIN(price) as i FROM product')->queryScalar();
        if($dataProvider->getCount()==0)
        {
            return $this->render('index',
                [
                    'categories'=> $categories,
                    'dataProvider' => $dataProvider,
                    'filtersCategory' => $filtersCategory,
                    'max' => (int)$max,
                    'min' => (int)$min
                ]);
        }
        else {
            return $this->render('index', [

                'categories'=> $categories,
                'dataProvider' => $dataProvider,
                'filtersCategory' => $filtersCategory,
                'max' => (int)$max,
                'min' => (int)$min
            ]);
        }
    }

    public function actionAddComment(){
        Yii::$app->response->format = 'json';

        $user_name=Yii::$app->user->getIdentity()->first_name;
        $user_name==''?$user_name='Аноним': $user_name;
        if(Yii::$app->request->post('id')){
            $comment=new Comment();
            $comment->object_id=Yii::$app->request->post('id');
            $comment->type=2;
            $comment->user_id=\Yii::$app->user->id;
            // $comment->user_id=Yii::$app->request->post('user_id');
            $comment->text=\Yii::$app->request->post('text');
            $comment->date=date("Y-m-d H:i:s");
            $comment->save();
            if($comment->save()){
                $data=[
                    'success'=>true,
                    'date'=>$comment->date,
                    'id'=>$comment->id,
                    'user'=>$comment->user_id,
                    'name'=>$user_name
                ];

            }else{
                $data=['success'=>$comment->errors];
            }
        }
        return $data;
    }

    public function actionDeleteComment(){
        Yii::$app->response->format = 'json';

        if(Yii::$app->request->post('id')){
            $comment=Comment::findOne(Yii::$app->request->post('id'))->delete();

            $data=['success'=>true,

            ];
        }else{
            $data=['success'=>false];
        }

        return $data;
    }

    public function actionRating(){
        \Yii::$app->response->format = 'json';
        Rating::deleteAll(['user_id'=>\Yii::$app->user->id,'object_id'=>\Yii::$app->request->post('object_id'), 'type'=>2]);
        $rating=new Rating();
        $rating->user_id=\Yii::$app->user->id;
        $rating->object_id=\Yii::$app->request->post('object_id');
        $rating->type=2;
        $rating->value=\Yii::$app->request->post('mark');
        if($rating->save()){
            $product=Product::findOne(['id'=>\Yii::$app->request->post('object_id')]);
            $product->updateRating();

            $data=['success'=>true,
                'rating'=>round($product->rating)
            ];

        }
        else{
            $data=['success'=>false];
        }

        return $data;
    }
    public function actionOrder(){
        \Yii::$app->response->format = 'json';
        $product=Product::findOne(['id'=>\Yii::$app->request->post('product_id')]);
        $model = new Task();
        $model->user_id=\Yii::$app->user->id;
        $model->email=\Yii::$app->user->getIdentity()->email;
        $model->sity=\Yii::$app->request->post('sity');
        $model->phone=\Yii::$app->request->post('phone');
        $model->state=2;
        $model->description=\Yii::$app->request->post('descrip');
        $model->date_added=date("Y-m-d H:i:s");
        $model->category_id=$product->category_id;
        $model->cooker_id=$product->user_id;


        if($model->save()){
            if($model->phone!=''){
                $user=User::findOne(Yii::$app->user->id);
                $user->phone=$model->phone;
                $user->save('false');
                $model->sendToCookerEmail($product);
            }

            $data=['success'=>true,

            ];

        }
        else{
            $data=['success'=>false,
                'errors'=>$model->errors];
        }

        return $data;
    }

}
