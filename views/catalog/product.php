<?php

/* @var $this yii\web\View */
/* @var $dataProvider ProductSearch */
/*$this->title = $id;
$this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/

use app\models\ProductSearch;
use app\models\Rating;
use yii\bootstrap\Html; ?>

<link rel="stylesheet" href="/css/product.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<?php foreach ($dataProvider->getModels() as $product):?>
    <div class="product col-md-10 col-md-offset-1  col-sm-12">
        <div class="product-images col-md-6 col-sm-12">
            <?php if($product->images):?>
                <?php foreach ($product->images as $image): ?>
                    <div class="mySlides" >
                        <img src="<?=$image? $image->url:'/images/notImage.png'?>" alt="image_<?=$image->id?>">
                    </div>
                <?php endforeach; ?>

                <div class="line">
                    <?php
                    $k = 0;
                    foreach ($product->images as $image): $k++; ?>
                        <div class="column">
                            <img class="demo cursor" src="<?=$image? $image->url:'/images/notImage.png'?>" alt="image_<?= $image->url ?>"onclick="currentSlide(<?= $k ?>)">
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif;?>
        </div>

        <div class="product-information col-md-4 col-sm-12">
            <h1><?= $product->name?></h1>
            <h2><?= $product->price?>руб</h2>
            <button class="custom_button"><a href="/seller/cooker/<?= $product->user->id?>" ><?= $product->user->first_name?></a></button>

            <?=(!\Yii::$app->user->isGuest && Yii::$app->user->id==$product->user->id)? Html::a('Редактировать', ['/cooker/product/update', 'id' => $product->id], ['class' => 'custom_button']) :''?>

            <?php if(!\Yii::$app->user->isGuest && Yii::$app->user->getIdentity()->user_type==1):?>
                <button class="custom_button"><a href="#popup">Хочу</a></button>

                <div id="stars">
                    <?php $has=Rating::findOne(['user_id'=>\Yii::$app->user->id,'object_id'=>$product->id]);
                    if(!is_object($has)/*&& !\Yii::$app->user->isGuest && \Yii::$app->user->getIdentity()->user_type==1*/):
                        ?>
                        <h4>Оцените изделие</h4>
                        <div class="rating-group">

                            <!-- <input class="rating__input rating__input--none" name="rating" id="rating-none" value="0" type="radio">
                             <label aria-label="No rating" class="rating__label" for="rating-none"><i class="rating__icon rating__icon--none fa fa-ban"></i></label>-->
                            <label aria-label="1 star" class="rating__label" for="rating-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                            <input class="rating__input" name="rating" id="rating-1" value="1" type="radio" >
                            <label aria-label="2 stars" class="rating__label" for="rating-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                            <input class="rating__input" name="rating" id="rating-2" value="2" type="radio" >
                            <label aria-label="3 stars" class="rating__label" for="rating-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                            <input class="rating__input" name="rating" id="rating-3" value="3" type="radio" >
                            <label aria-label="4 stars" class="rating__label" for="rating-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                            <input class="rating__input" name="rating" id="rating-4" value="4" type="radio" >
                            <label aria-label="5 stars" class="rating__label" for="rating-5"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                            <input class="rating__input" name="rating" id="rating-5" value="5" type="radio" >

                        </div>
                    <?php endif;?>
                </div>
            <?php endif;?>
            <?php if($product->rating!=0):?>
                <h4>Рейтинг</h4>
                <?php for($i=0;$i<round($product->rating);$i++):?>
                    <i class="rating__icon rating__icon--star fa fa-star" style="color: orange;"></i>
                <?php endfor;?>
            <?php endif;?>

        </div>
    </div>

<!--    <div id="options" class="col-md-10 col-md-offset-1">
        <ul id="product_nav" class="navbar-nav navbar-left nav">
            <li class="nav-item description"><a id="first" href="#description">Описание</a></li>
            <li class="nav-item com"><a href="#comments">Комментарии</a></li>
            <li class="nav-item tag"><a href="#tags">Теги</a></li>
        </ul>
    </div>-->
    <div class="col-sm-12 " style="height: 20px"></div>
    <div id="description" class="col-md-10 col-md-offset-1 col-sm-12">
               <?= $product->description? ' <h3>Описание</h3>'.$product->description:'Описания нет'?>
    </div>

    <div id="comments" class="col-md-10 col-md-offset-1 col-sm-12 ">
        <br>
        <?php if(!$product->comments):?>
            <h3>Комментариев пока нет</h3>
            <h5><?=\Yii::$app->user->isGuest? Html::a('Зарегистрируйтесь','/site/registration', ['title'=>'Регистрация']).' или '.Html::a('войдите','/site/login', ['title'=>'Вход']).' для добавления комментариев'
                    :''?></h5>
        <?php endif;?>
        <div id="com">
            <?php foreach ($product->comments as $comment): ?>

                <div class="comment_<?=$comment->id?>" id="<?=$comment->id?>" style="margin-left:0px; margin-bottom:10px; background-color: #eeeeee">
                    <div class="comment_information">
                        <span class="m-x-xs"><a href="/user/profile/<?=$comment->user->id?>" class="label label-success"><?=$comment->user->first_name==''?'Аноним':$comment->user->first_name?></a></span>
                        <?php if(!\Yii::$app->user->isGuest && ($comment->user->id==Yii::$app->user->id||\Yii::$app->user->getIdentity()->user_type==3)):?>
                            <span class="m-x-xs"><button style="border: none;   text-transform: unset; font-size: 75%;" value="<?=$comment->id?>" class="label label-danger delete_comment">Удалить</button></span>
                        <?php endif;?>
                        <span class="ml-xs"><i class="fa fa-clock-o"></i>&nbsp;<small><?=$comment->date?></small></span>
                    </div>
                    <div class="text wrap-comment"><?=$comment->text?></div>
                </div>

            <?php endforeach;?>
        </div>
        <?php if(!Yii::$app->user->isGuest):?>
            <div class="add_comment">
                <div class="col-md-9">
                    <input type="text" class="input" autocomplete="off" placeholder="Комментируйте" id="text_new_comment" name="new_comment">
                </div>
                <div class="col-md-1">
                    <button type="submit" id="add_new_comment" class="custom_button" >Комментировать</button>
                </div>
            </div>
        <?php endif;?>
    </div>

    <div id="tags" class="col-md-10 col-md-offset-1 col-sm-12 ">
        <?php if(!$product->tags):?>
            <h3>Тегов нет</h3>
        <?php else:?>
            <?php foreach ($product->tags as $tag):?>
                <a href='<?= \yii\helpers\Url::to(['/catalog/search', 'search_text' =>$tag->tag->name]) ?>'>#<?= $tag->tag->name?></a>
            <?php endforeach;?>
        <?php endif;?>
    </div>



    <!--<div class="popup" id="popup">
        <div class="popup-inner">
              <div class="popup__photo">
                  <img src="https://images.unsplash.com/photo-1515224526905-51c7d77c7bb8?ixlib=rb-0.3.5&s=9980646201037d28700d826b1bd096c4&auto=format&fit=crop&w=700&q=80" alt="">
              </div>
            <div class="popup__text">
                <h1>Оставьте заявку и кондитер свяжется с Вами!</h1>
                <?/*=\Yii::$app->user->isGuest?'Но сначала зарегистрируйтесь '. Html::a('Тут:)', ['/site/registration'], ['class' => 'custom_button']):''*/?>

                <label class="label">Телефон</label><input type="text" id="phone" class="input" name="phone" placeholder="+79787777777" autocomplete="off">

                <label class="label">Город</label><input type="text" id="sity" class="input" name="sity" placeholder="Симферополь" autocomplete="off">

                <label class="label" >Примечание</label><textarea id="descrip" class="input" name="descrip" rows="4" autocomplete="off"></textarea>
                <br>
                <button class="custom_button" id="send_order">Отправить</button>
            </div>
            <a class="popup__close" href="#">X</a>
        </div>
    </div>-->


    <div class="popup" id="popup">
        <div class="popup-inner">
            <!--  <div class="popup__photo">
                  <img src="https://images.unsplash.com/photo-1515224526905-51c7d77c7bb8?ixlib=rb-0.3.5&s=9980646201037d28700d826b1bd096c4&auto=format&fit=crop&w=700&q=80" alt="">
              </div>-->
            <div class="popup__text">
                <h1>Оставьте заявку и кондитер свяжется с Вами!</h1>
                <?=\Yii::$app->user->isGuest?'Но сначала зарегистрируйтесь '. Html::a('тут:)', ['/site/registration']):''?>
                <?php if(!Yii::$app->user->isGuest):?>
                <label class="label">Телефон</label><input type="text" id="phone" class="input" name="phone" placeholder="+79787777777" autocomplete="off">

                <label class="label">Город</label><input type="text" id="sity" class="input" name="sity" placeholder="Симферополь" autocomplete="off">

                <label class="label" >Примечание</label><textarea id="descrip" class="input" name="descrip" rows="3" autocomplete="off"></textarea>
                <br>
                <button class="custom_button" id="send_order">Отправить</button>
            </div>
            <?php endif;?>
            <a class="popup__close" href="#">X</a>
        </div>
    </div>

<?php endforeach;?>

<script>
    var slideIndex = 1;
    showSlides(slideIndex);

    // Next/previous controls
    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    // Thumbnail image controls
    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");
        // var captionText = document.getElementById("caption");
        if (n > slides.length) {
            slideIndex = 1
        }
        if (n < 1) {
            slideIndex = slides.length
        }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex - 1].style.display = "block";
        dots[slideIndex - 1].className += " active";
        // captionText.innerHTML = dots[slideIndex - 1].alt;
    }

    $(document).ready(function () {

/*        $(".description").click(function () {
            $("#description").toggle();
            /!*  $( "*" ).removeClass('active');
              $(this).addClass('active');*!/

        });
        $(".com").click(function () {
            $("#comments").toggle();
            /!*  $( "*" ).removeClass('active');
              $(this).addClass('active');*!/

        });
        $(".tag").click(function () {
            $("#tags").toggle();
            /!* $( "*" ).removeClass('active');
             $(this).addClass('active');*!/

        });*/


        $("#add_new_comment").on('click',function (){
            let text=$("#text_new_comment").val();

            $.post(
                "/catalog/add-comment",
                {
                    text: text,
                    id: <?=$id?>
                },
                function (data) {
                    if (data.success == true) {
                        $("#text_new_comment").val('');

                        $('#com').append(
                            "<div class=\"comment_"+data.id+"\"  id=\""+data.id+"\" style=\"margin-left:0px; margin-bottom:10px; background-color: #eeeeee\">\n" +
                            "            <div >\n" +
                            "                <div class=\"comment information\">\n" +
                            "                    <span class=\"m-x-xs\"><a href=\"/user/profile/"+data.user+"\" class=\"label label-success\">"+data.name+"</a></span>\n" +
                            "                    <span class=\"m-x-xs\"><button style=\"border: none; text-transform: unset; font-size: 75%;\" value=\""+data.id+"\" class=\"label label-danger delete_comment\">Удалить</button></span>\n" +
                            "                    <span class=\"ml-xs\"><i class=\"fa fa-clock-o\"></i>&nbsp;<small>"+data.date+"</small></span>\n" +
                            "                </div>\n" +
                            "                <div class=\"text wrap-comment\">"+text+"</div>\n" +
                            "            </div>\n" +
                            "        </div>"
                        );
                        $(".delete_comment").on('click',function (){
                            let id=$(this).val();
                            delComment(id);
                        });
                    } else if (data.success === false) {

                    }
                }
            );

        });

        $(".delete_comment").on('click',function (){
            let id=$(this).val();
            delComment(id);
        });

        function delComment(id) {
            $.post(
                "/catalog/delete-comment",
                {
                    id: id
                },
                function (data) {
                    if (data.success == true) {
                        $("#"+id).css('display','none');


                    } else if (data.success === false) {

                    }
                }
            );
        }


        /*$("  .mySlides>img").hover(function () {
            $(".demo").toggle();

        });*/

        $(".rating__input").on('click',function (){
            if(confirm('Подтвердите оценку')){
                let mark=$(this).val();


                $.post(
                    "/catalog/rating",
                    {
                        mark: mark,
                        object_id: <?=$id?>
                    },
                    function (data) {
                        if (data.success == true) {
                            $( "#stars" ).parent().append('<h4>Спасибо за оценку!</h4>');
                            $('#stars').hide();


                        } else if (data.success === false) {
                            /*  $( "#stars" ).parent().append('<h4>Что-то пошло не так !:( Перезагрузите страницу</h4>');
                              $('#stars').hide();*/

                        }
                    }
                );
            }

        });


        $("#send_order").on('click',function (){

            $.post(
                "/catalog/order",
                {
                    product_id:<?=$id?>,
                    phone:  $("#phone").val(),
                    descrip:  $("#descrip").val(),
                    sity:  $("#sity").val(),


                },
                function (data) {
                    if (data.success == true) {
                        $( ".popup__text" ).empty();
                        $( ".popup__text" ).append('<h1>Ваша заявка успешно отправляна!</h1>');


                    } else if (data.success === false) {


                    }
                }
            );

        });

    });
</script>

