<?php


/* @var $this yii\web\View */
/* @var $dataProvider app\models\ProductSearch*/

use yii\helpers\Html;

/*
$this->title = 'Каталог';
$this->params['breadcrumbs'][] = $this->title;*/
?>
<!--<h1><?/*= Html::encode($this->title) */?></h1>-->

<link rel="stylesheet" href="/css/catalog.css">
<?php if($filtersCategory):?>
    <div class=" catalog-index">

        <div class="search col-md-12 col-sm-12">
            <form method="get" action="/catalog/search">
                <div class="col-md-11 col-sm-11"> <input class="input" type="search" placeholder="Капкейки, шоколад, новый год"  autocomplete="off" name="search_text"></div>
                <div class="col-md-1  col-sm-1"><button class="button" id="search_button" type="submit">Поиск</button></div>
            </form>
        </div>

        <div class="col-md-12 col-sm-12" id="show_hide_filters_bar">

            <div class="col-sm-6">
                <h3>Фильтры
                    <button type="submit"  id="btn_filters">
                        <span  id="show_filters" class="glyphicon glyphicon-chevron-down"></span>
                        <span id="hide_filters" class="glyphicon glyphicon-chevron-up"></span>
                    </button>
                    <a href="/task/create" class="button">Заявка</a>
                </h3>
            </div>
        </div>

        <div class="col-sm-12" id="filter" <!--style="display: --><?/*=isset($params) && isset($params['filters'])? 'none':'block'*/?>">
        <form id="filters-form" role="form" method="get" action="/catalog"  >
            <?php foreach ($filtersCategory as $filterCategory): ?>
                <div class="col-sm-3">
                    <h4><?= $filterCategory->name ?></h4>
                    <div id="filter_<?= $filterCategory->name ?>">
                        <?php foreach ($filterCategory->filters as $item):
                            $checked = isset($params) && isset($params['filters']) && in_array($item->id,$params['filters']) ? 'checked' : '';
                            ?>
                            <div class="checkbox">
                                <label class="custom-checkbox"><input <?=$checked?> type="checkbox" name="filters[]" value="<?= $item->id ?>"><span><?= $item->name ?></span></label><!--<label class="custom-checkbox"><input  type="checkbox" name="filters[]" value="<?/*= $item->category_id*/?>[<?/*= $item->id */?>]"><span><?/*= $item->name */?></span></label>-->
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endforeach; ?>

            <div class="col-sm-3" >
                <h4>Категории</h4>
                <?php foreach ($categories as $category):
                    $checked = isset($params) && isset($params['category']) && in_array($category->id,$params['category']) ? 'checked' : '';?>
                    <div class="checkbox">
                        <label class="custom-checkbox">
                            <input  <?=$checked?> type="checkbox" name="category[]" value="<?= $category->id ?>"> <span><?= $category->name ?></span>
                        </label>
                    </div>
                <?php endforeach; ?>
            </div>

            <div class="col-sm-3">
                <h4>Сортировка</h4>
                <div class="radio">
                    <label class="custom-radio">
                        <input type="radio" <?=isset($params) && isset($params['sort']) && $params['sort']=='price_asc' ? 'checked' : '';?> name="sort" value="price_asc">
                        <span>По возрастанию цены</span>
                    </label>
                </div>
                <div class="radio">
                    <label class="custom-radio">
                        <input type="radio" <?=isset($params) && isset($params['sort']) && $params['sort']=='price_desc' ? 'checked' : '';?> name="sort" value="price_desc">
                        <span>По убыванию цены</span>
                    </label>
                </div>
                <div class="radio">
                    <label class="custom-radio">
                        <input type="radio" <?=isset($params) && isset($params['sort']) && $params['sort']=='name_asc' ? 'checked' : '';?> name="sort" value="name_asc">
                        <span>От А до Я</span>
                    </label>
                </div>
                <div class="radio">
                    <label class="custom-radio">
                        <input type="radio" <?=isset($params) && isset($params['sort']) && $params['sort']=='name_desc' ? 'checked' : '';?> name="sort" value="name_desc">
                        <span>От Я до А</span>
                    </label>
                </div>
            </div>


            <div class="col-sm-3">
                <h4>Цена</h4>
                <!--
                    <div id="slider">
                        <?/*= yii\jui\Slider::widget([
                            'clientOptions'=>
                                [
                                    'min'=>$min,
                                    'max'=>$max,
                                    'range'=>true,
                                    'values'=>[300, 500],
                                    'step'=>50
                                ]
                        ]) */?>
                    </div>-->
                <div class="input_price">
                    от<input type="number" id="min-price" name="min_price" value="<?=isset($params) && isset($params['min_price']) ? $params['min_price'] : $min;?>">
                    до<input type="number" id="max-price" name="max_price" value="<?=isset($params) && isset($params['max_price']) ? $params['max_price'] : $max;?>">
                </div>
                <br>
                <button type="submit" class="button">Применить</button>
            </div>

        </form>
    </div>
    </div>
<?php endif;?>
<?php if(\Yii::$app->request->get('search_text')):
    $str=\Yii::$app->request->get('search_text');
    $searchText= (new app\models\ProductSearch)->euToRu($str);
    ?>
    <?php if(preg_match("/^[a-z ]+$/i", $str)):?>
    <div class="container">
        <div class="col-sm-12">
            <h3>Показаны результаты по запросу <i><strong><?=$searchText?></strong></i></h3>

            <form method="get" action="/catalog/search">
                <input class="input" type="hidden" name="search_text_english" value="<?=$str?>">
                <h4><button type="submit">Искать по запросу<?=$str?></button></h4>
            </form>
        </div>
    </div>
<?php endif;?>
<?php endif;?>
<div class="col-sm-12" >
    <?php if($dataProvider):?>
        <?php
        $k = 0;
        foreach ($dataProvider->getModels() as $product):
            $k++; ?>
            <div class="col-md-3 col-sm-12 ">
                <a href="/catalog/product/<?= $product->id ?>" title="<?= $product->name ?>">
                    <div class="product <?= ($k % 4 == 0 ? 'product_no_float' : '') ?>">
                        <div class="product_image"><img src="<?=$product->image? $product->image->url:'/images/notImage.png'?>"></div>
                        <div class="product_name"><?= $product->name ?></div>
                        <div class="product_price"><?= $product->price ?> руб</div>
                        <div class="product_cooker"><a href="/seller/cooker/<?= $product->user->id?>"><?= $product->user->first_name ?></a></div>
                        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                        <?php if($product->rating!=0):?>
                            <?php for($i=0;$i<round($product->rating);$i++):?>
                                <i class="rating__icon rating__icon--star fa fa-star" style="color: orange;"></i>
                            <?php endfor;?>
                        <?php endif;?>
                        <?=(!\Yii::$app->user->isGuest && Yii::$app->user->id==$product->user->id)? Html::a('Редактировать', ['/cooker/product/update', 'id' => $product->id]) :''?>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>


<div class="col-sm-12" id="pager">
    <?php if($dataProvider->pagination->pageCount!=1)
        for ($i = 1; $i <= $dataProvider->pagination->pageCount; $i++) : ?>
            <a href="?page=<?= $i ?>"><?= $i ?></a>&nbsp;
        <?php endfor; ?>
</div>
<?php if($dataProvider->getCount()==0):?>
    <div class="container">
        <div class="col-sm-12">
            <h3>Ничего не найдено:(</h3>
            <h4>Вы можете<a href="/task/create" class="custom_button">добавить</a> заявку на уникальный заказ.</h4>
        </div>
    </div>
<?php endif; ?>


<script>

    $(document).ready(function () {

        $("#show_filters").css('display', 'none');


        $("#btn_filters").click(function () {
            $("#filter").toggle();
            $("#hide_filters").toggle();
            $("#show_filters").toggle();


        });

    });
    //var slider = new Slider('#ex2', {});
</script>
