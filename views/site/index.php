<?php

/* @var $this yii\web\View */

$this->title = 'Cakes Home';

use yii\helpers\Html; ?>

<link rel="stylesheet" href="/css/forIndex.css">

<div class="thumbnail"><img class="thumbnail__img" src="/images/categories/5.jpg" />
    <div class="thumbnail__content">
        <div class="thumbnail__title">CAKES HOME</div>
        <div class="thumbnail__subtitle">

                <form method="get" action="/catalog/search">
                    <input placeholder="Эклер, день рождения" name="search_text" autocomplete="off">
                    <button class="btn" title="Найти" aria-label="Search">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </button>
                </form>

        </div>
        <div class="thumbnail__link"></div>
    </div>
</div>
<div class="categories" >
    <?php foreach ($categories as $category):?>
        <div class="categorybox categorybox<?=$category->id?>">
            <div class="categoryName">
                <h3> <?= Html::a("$category->name", ['/catalog/category', 'id' => $category->id]) ?></h3>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<div class="cooker col-md-12 col-sm-12">
    <div class="image col-md-5 col-sm-12">
    <img src="/images/mlogo.png" alt="cooker" width="450">
    </div>
    <div class="text col-md-7 col-sm-12">
        <h1>Кондитер?</h1>
        <h3> <?= Html::a('Зарегистрируйся', ['/site/registration'], ['class' => '']) ?> за пару минут и получите доступ к личному кабинету. Размещайте свои десерты в каталоге, добавляте фотографии и получайте поток новых клиентов!  </h3>
    </div>
</div>

<script>
    $(document).ready(function () {
        $(window).scroll(function(){
            if($(window).width()>500) {
                if ($(window).scrollTop() > 120) {
                    $('.cooker').show('slow');
                } else $('.cooker').hide('fast');
            }

            if ($(window).scrollTop() > 110) {
                $('.navbar-fixed-top').css(' background', 'red');

            }

        });


    });

</script>