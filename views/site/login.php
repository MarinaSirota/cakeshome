<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/*$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;*/
?>
<link  rel="stylesheet" href="/css/login.css">

<div class="col-md-6 col-md-offset-3 col-xs-12">
    <div class="card">
        <div class="login-box">
            <div class="login-snip"> <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Вход</label>
                <!--<input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Регистрация</label>-->
                <div class="login-space">
                    <div class="login">

                        <?php $form = ActiveForm::begin([
                            'fieldConfig' => [
                                'template' => "<div class='group'><label class='label'>{label}</label>{input}{error}</div>",
                                'labelOptions' => ['class' => 'label'],
                            ],
                        ]); ?>

                        <?= $form->field($model, 'email')->textInput(['class' => 'input', 'placeholder'=>'Введите E-mail']) ?>

                        <?= $form->field($model, 'password')->passwordInput(['class'=>'input', 'placeholder'=>'Введите пароль'])->label('Пароль') ?>


                        <?= $form->field($model, 'rememberMe')->checkbox([
                            'template' =>  "<div class='group'>{input}<label for='check'><span class='icon'></span>{label}</label></div>",
                            'id'=>"check", 'class'=>"check"
                        ])
                            ->label('Запомнить меня')?>

                        <div class="group"> <input type="submit" class="button" value="Войти"> </div>
                        <?php ActiveForm::end(); ?>
                        <div class="hr"></div>
<!--                        <div class="foot"> <a href="#">Забыли пароль?</a> </div>-->
                        <div class="foot"> <a href="/site/registration" >Зарегистрироваться?</a> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


