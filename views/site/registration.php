<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form ActiveForm */
/*$this->title = 'Registration';
$this->params['breadcrumbs'][] = $this->title;*/
?>

<link  rel="stylesheet" href="/css/registration.css">
<div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-12 ">
        <div class="card">
            <div class="sign-up-box">
                <div class="sign-up-ship">
                    <input id="tab-1" type="radio" name="tab" class="sign-up-user" <?=$model->scenario=='cookers'? '':'checked'?>><label for="tab-1" class="tab">Регистрация</label>
                    <input id="tab-2" type="radio" name="tab" class="sign-up-cooker" <?=$model->scenario=='cookers'? 'checked':''?>><label for="tab-2" class="tab">Для кондитера</label>
                    <div class="sign-up-space">
                        <div class="sign-up">
                            <?php ($model->scenario = \app\models\User::SCENARIO_USER) ?>
                            <?php $form = ActiveForm::begin([
                                'fieldConfig' => [
                                    'template' => "<div class='group'><label class='label'>{label}</label>{input}{error}</div>",
                                    'labelOptions' => ['class' => 'label'],
                                ],
                            ]); ?>

                            <?= $form->field($model, 'email')->textInput(['class' => 'input', 'placeholder'=>'Введите E-mail','autocomplete'=>"off"]) ?>

                            <?= $form->field($model, 'password')->passwordInput(['class'=>'input', 'placeholder'=>'Введите пароль'])->label('Пароль') ?>
                            <input type="hidden" name="type" value="user">
                            <div class='group'><input type="hidden" name="userAgree" value="0"><input type="checkbox" class="check" name="userAgree" value="1" checked><label for='check'><span class='icon'></span><label for="check">  <a href="/site/agree">&nbsp;Согласие на обработку персональных данных</a></label></label></div>

                            <div class="group"> <input type="submit" class="button" value="Зарегистрироваться"> </div>
                            <?php ActiveForm::end(); ?>
                            <div class="hr"></div>
                        </div>
                        <div class="sign-up-cooker-form">
                            <?php ($model->scenario = \app\models\User::SCENARIO_COOKER) ?>
                            <?php $form = ActiveForm::begin([
                                'fieldConfig' => [
                                    'template' => "<div class='group'><label class='label'>{label}</label>{input}{error}</div>",
                                    'labelOptions' => ['class' => 'label'],
                                ],
                            ]); ?>



                            <?= $form->field($model, 'first_name')->textInput(['class' => 'input', 'placeholder'=>'Введите имя','autocomplete'=>"off"]) ?>

                            <?= $form->field($model, 'second_name')->textInput(['class' => 'input', 'placeholder'=>'Введите фамилию','autocomplete'=>"off"]) ?>

                            <?= $form->field($model, 'third_name')->textInput(['class' => 'input', 'placeholder'=>'Введите отчество','autocomplete'=>"off"]) ?>

                            <?= $form->field($model, 'inn_number')->textInput(['class' => 'input', 'placeholder'=>'Введите ИНН','autocomplete'=>"off"]) ?>

                            <?= $form->field($model, 'phone')->textInput(['class' => 'input', 'placeholder'=>'Введите телефон','autocomplete'=>"off"]) ?>

                            <?= $form->field($model, 'sity')->textInput(['class' => 'input', 'placeholder'=>'Введите город']) ?>

                            <?= $form->field($model, 'email')->textInput(['class' => 'input', 'placeholder'=>'Введите E-mail','autocomplete'=>"off"]) ?>

                            <?= $form->field($model, 'password')->passwordInput(['class'=>'input', 'placeholder'=>'Введите пароль'])->label('Пароль') ?>

                            <input type="hidden" name="type" value="cookers">
                            <div class='group'><input type="hidden" name="userAgree" value="0"><input type="checkbox" class="check" name="userAgree" value="1" checked><label for='check'><span class='icon'></span><label for="check"> <a href="/site/agree">&nbsp;Согласие на обработку персональных данных</a></label></label></div>
                            <div class="group"> <input type="submit" class="button" value="Зарегистрироваться"> </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2  mx-auto p-0" id="information">

    </div>
</div>

<script>
    $(document).ready(function () {

        $('#information').hide();

        $('.sign-up-ship>.sign-up-cooker').on('click', function () {
            $('#information').show();
        });

        $('.sign-up-ship>.sign-up-user').on('click', function () {
            $('#information').hide();
        });

    });


</script>

