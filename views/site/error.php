<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

/*$this->title = $name;*/
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <div class="col-sm-12" style="background: rgba(78,76,76,0.44) ; text-align: center">
        <h1> <img src="/images/error.png" width="100px" height="100px"> Упс! Такой страницы не существует:(</h1>
        <h4 style="text-align: center">
            Пожалуйста, свяжитесь с нами, если считаете, что это ошибка сервера. Спасибо.
        </h4>
    </div>


</div>
