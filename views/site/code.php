<?php



use yii\helpers\Html;

/*$this->title = 'Подтверждение';
$this->params['breadcrumbs'][] = $this->title;*/
?>

<link  rel="stylesheet" href="/css/login.css">
<div class="row">
    <div class="col-md-3  mx-auto p-0"></div>
    <div class="col-md-6 mx-auto p-0">
        <div class="card">
            <div class="login-box">
                <div class="login-snip"> <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Вход</label>
                    <div class="login-space">
                        <div class="login">
                            <?php if (isset($error)):?>
                            <div class="help-block-error"> <?=$error?></div>
                            <?php endif;?>
                                <form action="/site/check-key" method="post">
                                <input type="hidden" name="<?= \Yii:: $app->getRequest()->csrfParam ?>" value="<?= \Yii:: $app->getRequest()->getCsrfToken() ?>">
                                <div class="group"> <label class="label">Код </label> <input id="user" type="text" class="input" autocomplete="off" placeholder="Введите код" name="code" aria-required="true"> </div>
                                <div class="form-group">
                                    <div class="group"> <input type="submit" class="button" value="Подтвердить"> </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>