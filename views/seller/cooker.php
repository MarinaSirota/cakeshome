<?php

use app\models\Comment;
use app\models\Rating;
use yii\helpers\Html;


/* @var $this yii\web\View */


/*$this->title = $id;
$this->params['breadcrumbs'][] = $this->title;*/
?>

<link rel="stylesheet" href="/css/cooker.css">

<div class="card">
    <div class="col-md-4 col-sm-12 ">

        <div class="cooker_image">
            <img src="<?=$cooker->image? $cooker->image->url:'/images/notImage.png'?>" alt="image_<?=$id?>">
        </div>
    </div>
    <div class="information col-md-6 col-sm-12" >
        <div class="cooker_name"><h3><?=$cooker->second_name?> <?=$cooker->first_name?></h3></div>
        <div class="cooker_address"><?=Html::img('/images/address.png',['width'=>'20px', 'height'=>'20px'])?><?=$cooker->sity?>, <?=$cooker->address?></div>
        <div class="cooker_contacts">
            <p><?=!\Yii::$app->user->isGuest ? Html::img('/images/phone.png',['width'=>'20px', 'height'=>'20px']).''.$cooker->phone:Html::a('Зарегистрируйтесь для просмотра номера','/site/registration')?></p>
            <p><?=$cooker->instagram!=''? Html::img('/images/insta.png',['width'=>'20px', 'height'=>'20px']).''. $cooker->instagram:''?></p>
            <p><?=$cooker->site!=''? Html::img('/images/site.png',['width'=>'20px', 'height'=>'20px']).' '. $cooker->site:''?></p>
        </div>
        <div class="buttons">
            <button class="div_description"><a href="#cooker_description">О кондитере</a></button>
            <button class="div_comments"><a href="#cooker_comments">Комментарии</a></button>
            <button class="div_products"><a href="#cooker_products">Продукты</a></button>
        </div>
    </div>
    <div class="col-md-2 col-sm-12" >
        <?php if(!\Yii::$app->user->isGuest):?>
            <div id="stars">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                <?php $has=Rating::findOne(['user_id'=>\Yii::$app->user->id,'object_id'=>$cooker->id, 'type'=>1]);
                if(!is_object($has)&& !\Yii::$app->user->isGuest && \Yii::$app->user->getIdentity()->user_type!=2):
                    ?>
                    <h4>Оцените кондитера</h4>
                    <div class="rating-group">

                        <!-- <input class="rating__input rating__input--none" name="rating" id="rating-none" value="0" type="radio">
                         <label aria-label="No rating" class="rating__label" for="rating-none"><i class="rating__icon rating__icon--none fa fa-ban"></i></label>-->
                        <label aria-label="1 star" class="rating__label" for="rating-1"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                        <input class="rating__input" name="rating" id="rating-1" value="1" type="radio" >
                        <label aria-label="2 stars" class="rating__label" for="rating-2"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                        <input class="rating__input" name="rating" id="rating-2" value="2" type="radio" >
                        <label aria-label="3 stars" class="rating__label" for="rating-3"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                        <input class="rating__input" name="rating" id="rating-3" value="3" type="radio" >
                        <label aria-label="4 stars" class="rating__label" for="rating-4"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                        <input class="rating__input" name="rating" id="rating-4" value="4" type="radio" >
                        <label aria-label="5 stars" class="rating__label" for="rating-5"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                        <input class="rating__input" name="rating" id="rating-5" value="5" type="radio" >

                    </div>
                <?php endif;?>
            </div>
        <?php endif;?>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <?php if($cooker->rating!=0):?>
            <h4>Рейтинг</h4>
            <?php for($i=0;$i<round($cooker->rating);$i++):?>
                <i class="rating__icon rating__icon--star fa fa-star" style="color: orange;"></i>
            <?php endfor;?>
        <?php endif;?>
    </div>
</div>

<div id="cooker_description" class="col-md-12 col-sm-12">
    <?= $cooker->description? $cooker->description:'Описания нет'?>
</div>


<div id="cooker_comments" class="col-md-12">
    <?php if(!$cooker->comments):?>
        <h3>Комментариев пока нет</h3>
        <h5><?=\Yii::$app->user->isGuest? Html::a('Зарегистрируйтесь для добавления','/site/registration'):''?></h5>
    <?php endif;?>
    <div id="com">

        <?php $pr_coms=Comment::find()->where(['object_id'=>$pr_ids])->all();
        foreach ($pr_coms as $pr_com): ?>

            <div class="comment_<?=$pr_com->id?>" id="<?=$pr_com->id?>" style="margin-left:0px; margin-bottom:10px; background-color: #eeeeee">

                <div class="comment_information">
                    <span class="m-x-xs"><a href="/user/profile/<?=$pr_com->user->id?>" class="label label-success"><?=$pr_com->user->first_name==''?'Аноним':$pr_com->user->first_name?></a></span>
                    <span class="m-x-xs"><a href="/catalog/product/<?=$pr_com->object_id?>" class="label label-info" title="Перейти  к изделию">Изделие <?=$pr_com->object_id?></a></span>

                    <?php if(!\Yii::$app->user->isGuest && ($pr_com->user->id==Yii::$app->user->id||\Yii::$app->user->getIdentity()->user_type==3)):?>
                        <span class="m-x-xs"><button style="border: none;   text-transform: unset; font-size: 75%;" value="<?=$pr_com->id?>" class="label label-danger delete_comment">Удалить</button></span>
                    <?php endif;?>
                    <span class="ml-xs"><i class="fa fa-clock-o"></i>&nbsp;<small><?=$pr_com->date?></small></span>
                </div>
                <div class="text wrap-comment"><?=$pr_com->text?></div>
            </div>
        <?php endforeach;?>




        <?php foreach ($cooker->comments as $comment): ?>

            <div class="comment_<?=$comment->id?>" id="<?=$comment->id?>" style="margin-left:0px; margin-bottom:10px; background-color: #eeeeee">

                <div class="comment_information">
                    <span class="m-x-xs"><a href="/user/profile/<?=$comment->user->id?>" class="label label-success"><?=$comment->user->first_name==''?'Аноним':$comment->user->first_name?></a></span>
                    <?php if(!\Yii::$app->user->isGuest && ($comment->user->id==Yii::$app->user->id||\Yii::$app->user->getIdentity()->user_type==3)):?>
                        <span class="m-x-xs"><button style="border: none;   text-transform: unset; font-size: 75%;" value="<?=$comment->id?>" class="label label-danger delete_comment">Удалить</button></span>
                    <?php endif;?>
                    <span class="ml-xs"><i class="fa fa-clock-o"></i>&nbsp;<small><?=$comment->date?></small></span>
                </div>
                <div class="text wrap-comment"><?=$comment->text?></div>
            </div>
        <?php endforeach;?>
    </div>
    <?php if(!Yii::$app->user->isGuest):?>
        <div class="add_comment">

            <div class="col-md-9">
                <input type="text" class="input" autocomplete="off" placeholder="Комментируйте" id="text_new_comment" name="new_comment">
            </div>

            <div class="col-md-1">
                <button type="submit" id="add_new_comment" class="button" >Комментировать</button>
            </div>
        </div>
    <?php endif;?>
</div>

<div id="cooker_products" class="col-sm-12">
    <?php
    $k = 0;
    foreach ($products->getModels() as $product):
        $k++; ?>
        <div class="col-md-3 col-sm-12">
            <a href="/catalog/product/<?= $product->id ?>" title="Перейти ">
                <div class="product <?= ($k %4  == 0 ? 'product_no_float' : '') ?>" data-id="<?= $product->id ?>">
                    <div class="product_image"><img src="<?=$product->image? $product->image->url:'/images/notImage.png'?>"></div>
                    <div class="product_name"><?= $product->name ?></div>
                    <div class="product_price"><?= $product->price ?> руб/шт</div>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
</div>

<div id="pager">
    <?php for ($i = 1; $i <= $products->pagination->pageCount; $i++) : ?>
        <a href="?page=<?= $i ?>"><?= $i ?></a>&nbsp;
    <?php endfor; ?>
</div>


<script>
    $(document).ready(function () {


        $("button.div_description").click(function () {
            $("#cooker_description").toggle();
        });

        $("button.div_comments").click(function () {
            $("#cooker_comments").toggle();
        });
        /*    $("button.div_products").click(function () {
                $("#cooker_products").toggle();
            });
    */

        $("#add_new_comment").on('click',function (){
            let text=$("#text_new_comment").val();

            $.post(
                "/seller/add-comment",
                {
                    text: text,
                    id: <?=$id?>
                },
                function (data) {
                    if (data.success == true) {
                        $("#text_new_comment").val('');

                        $('#com').append(
                            "<div class=\"comment\" id=\""+data.id+"\" style=\"margin-left:0px; margin-bottom:10px; background-color: #eeeeee\">\n" +
                            "            <div >\n" +
                            "                <div class=\"comment information\">\n" +
                            "                    <span class=\"m-x-xs\"><a href=\"/user/profile/"+data.user+"\" class=\"label label-success\">"+data.name+"</a></span>\n" +
                            "                    <span class=\"m-x-xs\"><button style=\"border: none;  text-transform: unset; font-size: 75%;\" value=\""+data.id+"\" class=\"label label-danger delete_comment\">Удалить</button></span>\n" +
                            "                    <span class=\"ml-xs\"><i class=\"fa fa-clock-o\"></i>&nbsp;<small>"+data.date+"</small></span>\n" +
                            "                </div>\n" +
                            "                <div class=\"text wrap-comment\">"+text+"</div>\n" +
                            "            </div>\n" +
                            "        </div>"
                        );
                        $(".delete_comment").on('click',function (){
                            let id=$(this).val();
                            delComment(id);
                        });

                    } else if (data.success === false) {

                    }
                }
            );

        });

        $(".delete_comment").on('click',function (){
            let id=$(this).val();
            delComment(id);
        });

        function delComment(id) {
            $.post(
                "/catalog/delete-comment",
                {
                    id: id
                },
                function (data) {
                    if (data.success == true) {
                        $("#"+id).css('display','none');


                    } else if (data.success === false) {

                    }
                }
            );
        }

        $(".rating__input").on('click',function (){
            if(confirm('Подтвердите оценку')){
                let mark=$(this).val();


                $.post(
                    "/seller/rating",
                    {
                        mark: mark,
                        object_id: <?=$id?>
                    },
                    function (data) {
                        if (data.success == true) {
                            $( "#stars" ).parent().append('<h4>Спасибо за оценку!</h4>');
                            $('#stars').hide();


                        } else if (data.success === false) {
                            /*$( "#stars" ).parent().append('<h4>Спасибо за оценку!</h4>');
                            $('#stars').hide();*/
                        }
                    }
                );
            }

        });


    });
</script>

