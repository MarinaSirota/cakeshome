
<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

/*$this->title = 'Кондитеры';
$this->params['breadcrumbs'][] = $this->title;*/
?>

<link rel="stylesheet" href="/css/seller.css">
<div class="search col-md-12 col-sm-12 ">
    <form method="get" action="/seller/search">
        <div class="col-md-11 col-sm-11"> <input class="input" placeholder="Имя, фамилия, город" name="cooker_search_text"></div>
        <div class="col-md-1  col-sm-1"><button class="button" id="search_button" type="submit">Поиск</button></div>
    </form>
</div>

    <div class="col-sm-12">
        <?php $k=0;
        foreach ($dataProvider->getModels() as $seller):
            $k++;?>
        <div class="col-md-3 col-sm-12 ">
            <a href="/seller/cooker/<?= $seller->id?>" title="Перейти">
                <div class="cooker <?= ($k % 4 == 0 ? 'cooker_no_float' : '') ?>">
                    <div class="cooker_image"><img src="<?=$seller->image? $seller->image->url:'/images/notImage.png'?>" alt="image_<?=$seller->id?>"> </div>
                    <div class="cooker_name"><?= $seller->second_name?>  &nbsp; <?= $seller->first_name ?></div>
                    <div class="cooker_sity"><?= $seller->sity ?></div>
                    <div class="cooker_rating">
                        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                        <?php if($seller->rating!=0):?>
                        <?php for($i=0;$i<round($seller->rating);$i++):?>
                            <i class="rating__icon rating__icon--star fa fa-star" style="color: orange;"></i>
                        <?php endfor;?>
                        <?php endif;?>
                    </div>
                </div>
            </a>
        </div>
        <?php endforeach;?>
    </div>

<div class="col-sm-12" id="pager">
    <?php for ($i = 1; $i <= $dataProvider->pagination->pageCount; $i++) : ?>
        <a href="?page=<?= $i ?>"><?= $i ?></a>&nbsp;
    <?php endfor; ?>
</div>

