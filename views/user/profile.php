<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
?>


<div class="col-md-10 col-md-offset-1 col-sm-12" xmlns="http://www.w3.org/1999/html">
    <div class="user-image">
        <?=($model->image)? Html::img($model->image->url,['wight'=>'inherit','height'=>'100px']):'' ?>
    </div>
    <?php if($model->second_name!=''|| $model->first_name!=''):?>
        <div class="user-name"><h4><?=$model->second_name?> <?=$model->first_name?></h4></div>
    <?php endif;?>

    <?php if(\Yii::$app->user->id==$model->id):?>
        <div class="user-email"><h4>Email:<?=$model->email?></span></h4></div>
        <?php if($model->phone!=''):?>
            <div class="user-phone"><h4>Телефон:<?=$model->phone?></span></h4></div>
        <?php endif;?>
        <?= Html::a('Редактировать профиль', ['update', 'id' => $model->id], ['class' => 'custom_button'])?>
        <?= Html::a('Добавить заявку', ['/task/create'], ['class' => 'custom_button'])?>
    <?php endif;?>
</div>

<?php if(\Yii::$app->user->getIdentity()->user_type==1):?>
    <?php if(\Yii::$app->user->id==$model->id):?>
        <div class="col-sm-12 col-md-10 col-md-offset-1 tasks">
            <h3> Заявки </h3>
            <?php if(is_array($model->tasks)):
                foreach ($model->tasks as $task):?>
                    <a  href="/task/view/?id=<?=$task->id?>" title="Подробнее">&nbsp;
                        <div class="col-sm-12 task">
                            <?=mb_substr($task->description, 0, 100)?><?=mb_strlen($task->description)>100?'...':''?>
                        </div>
                    </a>
                <?php endforeach;?>


            <?php else:?>
                <h2> У вас нет индивидуальных заявок</h2>
            <?php endif;?>
        </div>
    <?php endif;?>
<?php endif;?>

<style>
    .tasks a,  .tasks a:hover,  .tasks a:focus,  .tasks a:active{
        text-decoration: none;
    }
    .task{
        color: #0a0a0a;
        border-radius: 10px;
        margin-bottom:10px;
        padding: 15px;
        background-color: #eeeeee;
        transition: all 0.5s ease 0s;
        box-shadow: 0px 5px 5px 0px rgba(186, 126, 126, .5)


    }
    .task:hover {
        cursor: pointer;
        box-shadow:5px 5px 15px 10px rgba(186, 126, 126, .5);
        /*    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 1);*/
    }
</style>