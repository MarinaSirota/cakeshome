<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */

?>
<div class="user-update col-sm-3">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="user-form">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

        <?= Html::img($model->image->url,['wight'=>'inherit','height'=>'100px']) ?>

        <?= $form->field($model, 'photo')->fileInput() ?>

        <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
        <input type="hidden" name="type" value="user">
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-info']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>
