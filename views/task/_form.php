<?php

use app\models\Category;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<link  rel="stylesheet" href="/css/task.css">

<div class="col-md-10 col-md-offset-1 mx-auto p-0 col-sm-12">
    <div class="card">
        <div class="task-box">
            <div class="task-snip"> <label for="tab-1" class="tab">Заявка</label>
                <div class="task-space">
                    <div class="task">

                        <?php $form = ActiveForm::begin([
                            'fieldConfig' => [
                                'template' => "<div class='group'><label class='label'>{label}</label>{input}{error}</div>",
                                'labelOptions' => ['class' => 'label'],
                            ],
                        ]); ?>

                        <?= $form->field($model, 'phone')->textInput(['class' => 'input', 'placeholder'=>'Не обязательно', 'autocomplete'=>'off']) ?>

                        <?= $form->field($model, 'sity')->textInput(['class' => 'input', 'placeholder'=>'Симферополь',  'autocomplete'=>'off']) ?>

                        <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Category::find()->all(), 'id', 'name'),['class'=>'input']) ?>

                        <?= $form->field($model, 'description')->textarea(['rows' => 6,'class'=>'input', 'autocomplete'=>'off']) ?>

                        <?= $form->field($model, 'date')->textInput(['class' => 'input', 'placeholder'=>'',  'autocomplete'=>'off']) ?>

                        <?= $form->field($model, 'min_price')->textInput(['class' => 'input', 'placeholder'=>'Не обязательно', 'autocomplete'=>'off']) ?>

                        <?= $form->field($model, 'max_price')->textInput(['class' => 'input', 'placeholder'=>'Не обязательно', 'autocomplete'=>'off']) ?>

                        <?= $form->field($model, 'delivery',['inputOptions' => ['class' => 'input']])->dropDownList([ 1 => 'Любой', 2 => 'Доставка', 3 => 'Самовывоз'],['prompt' => '---','class'=>'input']) ?>

                        <br>

                        <div class="group"> <input type="submit" class="button" value="Готово"> </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('option').addClass('input');

    });
</script>