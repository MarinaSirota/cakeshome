<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap4;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="task-index">

    <!--    <p>
        <?/*= Html::a('Create Task', ['create'], ['class' => 'btn btn-success']) */?>
    </p>
-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="row">

        <div class="col-md-12 col-sm-12">
            <?php foreach ($dataProvider->getModels() as $task):?>
                <?php $k=0;
                if($task->state==1):
                    $k++;?>
                    <div class="col-md-3">
                        <div class="thumbnail">
                            <div class="caption text-center">
                                <div class="position-relative">
                                </div>
                                <h4 id="thumbnail-label"><?=$task->category->name?></h4>
                                <p><a href="/user/profile/<?=$task->user_id?>" target="_blank"><i class="glyphicon glyphicon-user light-red lighter bigger-120"></i>&nbsp;<?=$task->user->first_name!=''?$task->user->first_name:'Пользователь'.$task->user_id?></a></p>
                                <div class="thumbnail-description smaller"><?=mb_substr($task->description, 0, 256)?><?=mb_strlen($task->description)>256?'...':''?></div>
                            </div>
                            <div class="caption card-footer text-center">
                                <ul class="list-inline">
                                    <li><i class="date"></i>&nbsp;<?=$task->date?> </li>
                                    <li></li>
                                    <li><i class="glyphicon glyphicon-envelope lighter"></i><a href="/task/view/?id=<?=$task->id?>">&nbsp;Посмотреть</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php endif;?>
            <?php endforeach;?>
            <?php /*if($k==0):*/?><!--
                <h2 class="nothing">Нет актульаных заказов</h2>
            --><?php /*endif;*/?>
        </div>
    </div>

    <style>
        body {
            margin: 0;
            background-image: url("/images/categories/5(1).jpg");
            background-size: cover;
            background-repeat: no-repeat;
            /*  color: #6a6f8c;
              background : #c8c8c8;
              font: 600 16px/18px 'Open Sans', sans-serif*/
        }
        .nothing{
            color: #FFFFFF;
            font-weight: 600;
            background-color: rgba(78,76,76,0.5);
            border-radius: 10px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.5);
            padding: 10px;
            text-align: center;

        }
        .thumbnail {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.5);
            transition: 0.3s;
            min-width: 40%;
            border-radius: 5px;
        }

        .thumbnail-description {
            min-height: 40px;
        }

        .thumbnail:hover {
            cursor: pointer;
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 1);
        }

    </style>


</div>


