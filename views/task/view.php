<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Task */

/*$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
\yii\web\YiiAsset::register($this);
?>
<div class="task-view">


    <p>
        <?php if($model->user_id==\Yii::$app->user->id):?>
            <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'custom_button']) ?>
            <?= Html::a('Отменить', ['delete', 'id' => $model->id], [
                'class' => 'custom_button',
                'data' => [
                    'confirm' => 'Подтвердите отмену',
                    'method' => 'post',
                ],
            ]) ?>
            <?php if($model->state==3):?>
                <?= Html::a('Подтвердить выполнение', ['completeUser', 'id' => $model->id], ['class' => 'custom_button', 'data' => [
                    'confirm' => 'Уверены, что хотите подтвердить выполнение?']]) ?>
            <?php endif;?>
        <?php endif;?>
        <?php if(\Yii::$app->user->getIdentity()->user_type==2 && $model->state==1):?>
            <?= Html::a('Взять заказ', ['busy', 'id' => $model->id], ['class' => 'custom_button', 'data' => [
                'confirm' => 'Уверены, что хотите взять заказ?']]) ?>
        <?php endif;?>
        <?php if(\Yii::$app->user->getIdentity()->user_type==2 && $model->state==2):?>
            <?= Html::a('Подтвердить выполнение', ['completeCooker', 'id' => $model->id], ['class' => 'custom_button', 'data' => [
                'confirm' => 'Уверены, что хотите подтвердить выполнение?']]) ?>
        <?php endif;?>
    </p>

    <div class="col-sm-3">
        <h3> Сроки</h3>
        <div><?=$model->date?></div>
        <h3> Цена </h3>
        <div>от&nbsp;<?=$model->min_price?>&nbsp;до&nbsp;<?=$model->max_price?></div>
    </div>


    <div class="col-sm-3">
        <h3>Город</h3>
        <div><?=$model->sity?></div>
        <h3> Способ доставки</h3>
        <div><?php
                if($model->delivery!=''){
                    if ($model->delivery==1){
                        echo 'Любой';
                    }
                    if ($model->delivery==2){
                        echo 'Доставка';
                    }
                    if ($model->delivery==3){
                        echo 'Самовывоз';
                    }
                }

        ?></div>
        <h3>Контакты</h3>
        <div><?=$model->phone!=0?$model->phone:'' ?></div>
        <div><?=$model->email?></div>

    </div>

    <div class="col-sm-6">
        <h3> Описание </h3>
        <div><?=$model->description?></div>
    </div>


</div>
