<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Task */

/*$this->title = 'Create Task';
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="task-create">

    <h2>Заявка на уникальный десерт</h2>

    <?php if(!isset($model)):?>
        <h2>Ваша заявка успешно добавлена!  </h2>
        <p>Как только кондитер возьмет Ваш заявку, Вам придет уведомление на почту!</p>
        <p> Историю Ваших заявок можете посмотреть в профиле </p>

    <?php else:?>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    <?php endif;?>



</div>
