<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form ActiveForm */
?>
<div class="registration">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'first_name') ?>
        <?= $form->field($model, 'second_name') ?>
        <?= $form->field($model, 'third_name') ?>
        <?= $form->field($model, 'sity') ?>
        <?= $form->field($model, 'address') ?>
        <?= $form->field($model, 'description') ?>
        <?= $form->field($model, 'phone') ?>
        <?= $form->field($model, 'instagram') ?>
        <?= $form->field($model, 'site') ?>
        <?= $form->field($model, 'block') ?>
        <?= $form->field($model, 'inn_number') ?>
        <?= $form->field($model, 'user_type') ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'password') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- registration -->
