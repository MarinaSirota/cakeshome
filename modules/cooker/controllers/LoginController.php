<?php

namespace app\modules\cooker\controllers;

use app\models\UserForm;
use Yii;


/**
 * Default controller for the `admin` module
 */
class LoginController extends CookerController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function beforeAction($action)
    {
        return parent::beforeAction('login');
    }

    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/cooker');
        }

        $model = new UserForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/cooker');
        }

        $model->password = '';
        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
