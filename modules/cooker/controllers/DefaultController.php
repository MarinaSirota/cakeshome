<?php

namespace app\modules\cooker\controllers;


/**
 * Default controller for the `CookerModule` module
 */
class DefaultController extends CookerController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

}
