<?php

use dosamigos\tinymce\TinyMce;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="user-form row " >

    <div class="col-sm-3">
        <h3> ФИО </h3>
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

        <?= Html::img($model->image->url,['wight'=>'inherit','height'=>'100px']) ?>

        <?= $form->field($model, 'photo')->fileInput() ?>

        <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'second_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'third_name')->textInput(['maxlength' => true]) ?>
    </div>


    <div class="col-sm-3">
        <h3> Местоположение </h3>

        <?= $form->field($model, 'sity')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

        <h3> Способы связи </h3>
        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'instagram')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'site')->textInput(['maxlength' => true, ]) ?>
    </div>




    <div class="col-sm-6">
        <h3> Описание </h3>

        <?= $form->field($model, 'description')->widget(TinyMce::className(), [
            'options' => ['rows' => 20],
            'language' => 'ru',
            'clientOptions' => [
                'plugins' => [
                    "advlist autolink lists link charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste"
                ],
                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
            ]
        ]);?>


        <div class="form-group">

            <?= Html::submitButton('Сохранить', ['class' => 'custom_button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

