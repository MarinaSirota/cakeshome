<?php

/* @var $this yii\web\View */
?>


<div class="col-sm-12 tasks">
    <h3> Заявки </h3>
    <?php if(is_array($model->tasks)):
        foreach ($model->tasks as $task):?>
            <a  href="/task/view/?id=<?=$task->id?>" title="Подробнее">&nbsp;
                <div class="col-sm-12 task">
                    <?=mb_substr($task->description, 0, 100)?><?=mb_strlen($task->description)>100?'...':''?>
                    <p class="pull-right"><?=$task->date_added?></p>
                </div>
            </a>
        <?php endforeach;?>

    <?php else:?>
        <h2> У вас нет индивидуальных заявок</h2>
    <?php endif;?>
</div>


<style>
    .tasks a,  .tasks a:hover,  .tasks a:focus,  .tasks a:active{
        text-decoration: none;
    }
    .task{
        color: #0a0a0a;
        border-radius: 10px;
        margin-bottom:10px;
        padding: 15px;
        background-color: #eeeeee;
        transition: all 0.5s ease 0s;
        box-shadow: 0px 0px 5px 0px rgba(186, 126, 126, .5)


    }
    .task:hover {
        cursor: pointer;
        box-shadow:0px 0px 15px 5px rgba(186, 126, 126, .5);
        /*    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 1);*/
    }
</style>