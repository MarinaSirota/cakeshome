<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Личный кабинет';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $dataProvider->id], ['class' => 'custom_button']) ?>
    </p>




    <div class="col-sm-3">
        <h3> ФИО </h3>

        <?= Html::img($dataProvider->image->url,['wight'=>'inherit','height'=>'100px']) ?>


        <div><?=$dataProvider->first_name?></div>
        <div><?=$dataProvider->second_name?></div>
        <div><?=$dataProvider->third_name?></div>
    </div>


    <div class="col-sm-4">
        <h3> Местоположение </h3>

        <div><?=$dataProvider->sity?></div>
        <div><?=$dataProvider->address?></div>
        <h3> Способы связи </h3>
        <div><?=$dataProvider->phone?></div>
        <div><?=$dataProvider->instagram?></div>
        <div><?=$dataProvider->site?></div>

    </div>




    <div class="col-sm-4">
        <h3> Описание </h3>
        <div><?=$dataProvider->description?></div>
    </div>








</div>
