<div class="index">

    <div class="col-md-6 col-sm-12">
        <img src="/images/shef.jpg" alt="cooker" width="500px">
    </div>
    <div class="col-md-4 col-md-offset-1 col-sm-12">
        <h3><?=\Yii::$app->user->getIdentity()->first_name?>, рады видеть Вас снова ♥</h3>
        <ul class="nav nav-pills nav-stacked">
            <li><a href="/cooker/product/create" > <button class="custom_button">Добавить изделие</button></a></li>
            <li><a  href="/cooker/user/update?id=<?=\Yii::$app->user->id?>"><button class="custom_button">Редактировать профиль</button></a></li>
            <li><a href="/task/index"> <button class="custom_button">Посмотреть ленту заявок</button></a></li>
            <li><a href="/cooker/user/task"> <button class="custom_button">Ваши заявки</button></a></li>
            <!--<li><button href="/task" class="button">Посмотреть комментарии</button></li>-->
        </ul>
    </div>

</div>
<style>
    @import url('https://fonts.googleapis.com/css?family=Lato:400,700');
    h3{
        text-align: center;
        font-family: 'Lato', sans-serif, sans-serif;
    }

    .custom_button{

        width: 100%;
    }
    .index a{
        text-decoration: none;
        margin: 0;
        padding: 0 !important;
    }
    @media (max-width: 500px){
        img{

            max-width:300px ;
        }
    }


</style>