<?php

use app\models\Category;
use app\models\Tag;
use app\models\Filter;
use app\models\FilterCategory;
use dosamigos\tinymce\TinyMce;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
/* @var $tags app\models\Tag */
/* @var $filtersCategory app\models\FilterCategory */

$tagsArray =  $model->checkedTagsArray();
$filtersArray =  $model->checkedFiltersArray();
?>
<div class="product-form col-sm-12">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Category::find()->all(), 'id', 'name'))->label('Категория') ?>

    <?= $form->field($model, 'description')->widget(TinyMce::className(), [
        'options' => ['rows' => 20],
        'language' => 'ru',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>



    <div class="container">
        <?php foreach($model->images as $image):?>
            <div style="float:left; margin: 0 5px 0" id="<?=$image->id?>">
                <?= Html::img($image->url,['wight'=>'auto','height'=>'120px']) ?>
                <div>
                    <?php if($image->main==1):?>
                    <a class="main-photo-temp"  data-id="<?=$image->id?>" style=" cursor: pointer; border:none; background-image: none; background-color: transparent;" aria-label="Update">
                        <span class="glyphicon glyphicon-ok-circle" data-id="<?=$image->id?>" title="Главное фото" aria-hidden="true"></span>
                    </a>
                    <?php else :?>

                        <a class="main-photo-new"  data-id="<?=$image->id?>" style=" cursor: pointer; border:none; background-image: none; background-color: transparent;"  aria-label="Update">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true" title="Сделать главным фото"></span>
                        </a>
                    <?php endif;?>

                    <a  class="delete-photo" data-id="<?=$image->id?>" style="cursor: pointer; border:none; background-image: none; background-color: transparent;" title="Удалить" aria-label="Update">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    </a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <?= $form->field($model, 'photo[]')->fileInput(['multiple' => true, count($model->images)<\app\models\Product::MAX_IMAGES? 'disabled':''])->hint('Не более '.(\app\models\Product::MAX_IMAGES-count($model->images)).' фото') ?>


    <?= $form->field($model, 'tag')
        ->checkboxList(ArrayHelper::map(Tag::find()->all(), 'id', 'name'), [
            'item' => function ($index, $label, $name, $checked, $value) use ($tagsArray) {
                $checked = in_array($value, $tagsArray) ? 'checked' : '';
                return "<label class='custom-checkbox'>
                        <input type='checkbox' {$checked} value='{$value}' name='$name'> <span>    {$label}</span>
                    </label>";
            }
        ]);
    ?>

    <?= $form->field($model, 'new_tags')->textInput(['placeholder'=>'День матери, с манго, для диабетиков'])->hint('Чтобы добавить новые теги, введите их через запятую') ?>


    <?php $filtersCategory = FilterCategory::find()->all();
    foreach ($filtersCategory as $filterCategory): ?>
        <div class="col-sm-2">
            <h4><?= $filterCategory->name ?></h4>

                <?php foreach ($filterCategory->filters as $item):
                    $checked = in_array($item->id, $filtersArray) ? 'checked' : '';
                    ?>
                    <div class="checkbox">
                        <label class="custom-checkbox"><input <?=$checked?> type="checkbox" name="Product[filter][]" value="<?= $item->id ?>"><span><?= $item->name ?></span></label><!--<label class="custom-checkbox"><input  type="checkbox" name="filters[]" value="<?/*= $item->category_id*/?>[<?/*= $item->id */?>]"><span><?/*= $item->name */?></span></label>-->
                    </div>
                <?php endforeach; ?>

        </div>
    <?php endforeach; ?>



    <div class="col-sm-12">
        <?= Html::submitButton('Сохранить', ['class' => 'custom_button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    $(document).ready(function () {

        $(".delete-photo").on('click', function () {
            let id=$(this).data('id');
            $.post(
                "/cooker/product/delete-photo",
                {
                    id: $(this).data('id')
                },
                function (data) {
                    if (data.success == true) {
                        $('#'+id).hide();
                    }

                }
            );

        });

        $(".main-photo-new").on('click', function () {

            let newid=$(this).data('id');
            //let oldid=$('[class *="glyphicon-ok-circle"]').data('id');
            let oldid=$('.main-photo-temp').data('id');
            changeMainPhoto(newid, oldid);

        });

        function changeMainPhoto(newid, oldid) {
            $.post(
                "/cooker/product/main-photo",
                {
                    old_main_photo_id:oldid,
                    new_main_photo_id: newid

                },
                function (data) {
                    if (data.success === true) {
                        $("#"+newid+" .main-photo-new span").removeClass('glyphicon glyphicon-ok').addClass('glyphicon glyphicon-ok-circle').prop('title', 'Главное фото');
                        $("#"+oldid+" .main-photo-temp span").removeClass('glyphicon glyphicon-ok-circle').addClass('glyphicon glyphicon-ok').prop('title', 'Чтобы сделать главным фото, перезагрузите страницу');
                        $("#"+newid+" .main-photo-new").removeClass('main-photo-new').addClass('main-photo-temp');
                        $("#"+oldid+" .main-photo-temp").removeClass('main-photo-temp').addClass('main-photo-new');
                       /* $("#"+newid+" .main-photo-temp").data('id',newid );
                        $("#"+oldid+" .main-photo-new").data('id',oldid );*/
                        // $("#"+oldid).append(' <a class="main-photo"  data-id=\"+oldid+\" style=" cursor: pointer; border:none; background-image: none; background-color: transparent;" title="Сделать главным фото" aria-label="Update">\n' +
                        //     '                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>\n' +
                        //     '                        </a>');
                        //
                        // $("#"+newid).append('<span class="glyphicon glyphicon-ok-circle" data-id=\"+newid+\" title="Главное фото" aria-hidden="true"></span>');
                        //$('#'+old_main_photo_id+>'.main-photo>span).removeClass();
                        //$(".main-photo>span").addClass('glyphicon glyphicon-ok-circle');
                        //$("#"+new_id+>".main-photo>span").removeClass('glyphicon glyphicon-ok');
               /*         $(".main-photo-new").on('click', function () {

                            let newid=$(this).data('id');
                            //let oldid=$('[class *="glyphicon-ok-circle"]').data('id');
                            let oldid=$('.main-photo-temp').data('id');
                            changeMainPhoto(newid, oldid);

                        });*/

                    }

                }
            );

        }


    });

</script>
