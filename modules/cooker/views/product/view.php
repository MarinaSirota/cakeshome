<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Изделия', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'custom_button']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'custom_button',
            'data' => [
                'confirm' => 'Удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'description',
            'price',
            [   'label' => 'Категория',
                'attribute'=>'category_id',
                'value'=>$model->category->name,
            ],
            [   'label'=>'Фото',
                'format' => 'html',
                'value'=> function($model){
                    $array='';
                    foreach ($model->images as $image) {
                        $array=$array.Html::img($image->url, ['wight' => '100px', 'height' => '100px']);
                    }
                    return $array;
                },

            ],
            [   'label'=>'Теги',
                'value'=> function($model){
                    $array='';
                    foreach ($model->tags as $tag) {
                        $array=$array.$tag->tag->name.'; ';
                    }
                    return $array;
                },

            ],
        ],

    ]) ?>

</div>
