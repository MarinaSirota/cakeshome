<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>

<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => [

            [
                'label' => 'Личный кабинет',
                'url' => ['/cooker/user']
            ],
            [
                'label' => 'Изделия',
                'url' => ['/cooker/product']
            ],
            [
                'label' => 'Заявки',
                'url' => ['/cooker/user/task']
            ],
            [
                'label' => 'Каталог',
                'url' => ['/catalog']
            ],
            [
                'label' => 'Кондитеры',
                'url' => ['/seller']
            ],
            [
                'label' => 'О нас',
                'url' => ['/site/about']
            ],
            [
                'label' => 'Вход',
                'url' => ['/site/login'],
                'visible' => \Yii::$app->user->isGuest
            ],
            [
                'label' => 'Регистрация',
                'url' => ['/site/registration'],
                'visible' => \Yii::$app->user->isGuest
            ],
            [
                'label' => Html::img('@web/images/userProfile.png', ['wight' => '20px', 'height' => '20px']),
                'url' => [!\Yii::$app->user->isGuest ? ( \Yii::$app->user->getIdentity()->user_type==2 ? '/cooker':'/user/profile') : '/', 'id' => Yii::$app->user->id],
                'visible' => !\Yii::$app->user->isGuest
            ],
            [
                'label' =>   Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        Html::img('@web/images/exit.png',['wight'=>'20px','height'=>'20px']),
                        ['class' => 'btn btn-link logout','title'=>'Выйти', 'style' => ['border' => 'none', 'background-color' => 'transparent']]
                    )
                    . Html::endForm(),
                'url' => ['/site/logout'],
                'visible' => !\Yii::$app->user->isGuest

            ],

        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Cakes Home <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
