<?php

namespace app\modules\admin\controllers;


/**
 * Default controller for the `AdminModule` module
 */
class DefaultController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

}
