<?php
/**
 * Created by PhpStorm.
 * User: sm_fe
 * Date: 23.05.2019
 * Time: 22:57
 */

namespace app\modules\admin\controllers;


use yii\web\Controller;

class AdminController extends Controller
{
    public function init()
    {
        parent::init(); // TODO: Change the autogenerated stub
        $this->layout = 'admin';
    }

    public function beforeAction($action)
    {
        if(\Yii::$app->user->isGuest && $action != 'login') {
            return $this->redirect('/admin/login');
        }
        if(!\Yii::$app->user->isGuest && \Yii::$app->user->getIdentity()->user_type!=3 && $action!='login') {
            return $this->redirect('/');
        }
        /*if(!\Yii::$app->user->isGuest && \Yii::$app->user->identity->user_type!=3 && $action!='login') {
            return $this->redirect('/admin/login/index');
        }*/

        return parent::beforeAction($action);
    }
}