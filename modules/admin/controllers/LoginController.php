<?php

namespace app\modules\admin\controllers;

use app\models\LoginForm;
use app\models\UserForm;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class LoginController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function beforeAction($action)
    {
        return parent::beforeAction('login');
    }

    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/admin');
        }

        $model = new UserForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/admin');
        }

        $model->password = '';
        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
