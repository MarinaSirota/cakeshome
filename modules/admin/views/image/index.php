<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ImageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Картинки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-index">

    <h1><?= Html::encode($this->title) ?></h1>

  <!--  <p>
        <?/*= Html::a('Create Image', ['create'], ['class' => 'btn btn-success']) */?>
    </p>-->

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary'=>'Показано {begin}-{end} из {totalCount} ',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'object_id',
            [
                'label' => 'Тип',
                'attribute'=>'type',
                'value'=>function($model){
                    if($model->type==1){
                        return 'Пользователь';
                    }
                    if($model->type==2){
                        return 'Изделие';
                    }

                }
            ],
            'url',
            [
                'label' => 'Главное',
                'attribute'=>'main',
                'value'=>function($model){
                    if($model->main==1){
                        return 'Да';
                    }
                    if($model->main==0){
                        return 'Нет';
                    }

                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Действия',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => '{view}{update}{delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'Посмотреть'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Редактировать'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Удалить'),
                            'data-confirm' => Yii::t('yii', 'Подтвердите удаление?'),
                            'data-method'  => 'post',

                        ]);
                    }


                ],
            ],
        ],
    ]); ?>


</div>
