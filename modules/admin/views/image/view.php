<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Image */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Фото', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="image-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Подтвердите удаление',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'object_id',
            [
                'label' => 'Тип',
                'attribute'=>'type',
                'value'=>function($model){
                    if($model->type==1){
                        return 'Пользователь';
                    }
                    if($model->type==2){
                        return 'Изделие';
                    }

                }
            ],
            'url',
            [
                'label' => 'Главное',
                'attribute'=>'main',
                'value'=>function($model){
                    if($model->main==1){
                        return 'Да';
                    }
                    if($model->main==0){
                        return 'Нет';
                    }

                }
            ],
        ],
    ]) ?>

</div>
