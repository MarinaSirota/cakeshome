<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Изделия';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary'=>'Показано {begin}-{end} из {totalCount} ',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            'description',
            'price',
            [   'label' => 'Категория',
                'attribute'=>'category_id',
                'value'=>function($dataProvider){

                    return $dataProvider->category->name;
                }
            ],
            [   'label' => 'Кондитер',
                'attribute'=>'user_id',
                'format'=>'raw',
                'value'=>function($dataProvider){

                    return Html::a($dataProvider->user_id, "/admin/user/view?id=$dataProvider->user_id");
                        //$dataProvider->user->first_name.PHP_EOL.'id='.$dataProvider->user_id ;
                }
            ],
            //'user_id',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Действия',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => '{view}{update}{delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'Посмотреть'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Редактировать'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Удалить'),
                            'data-confirm' => Yii::t('yii', 'Подтвердите удаление?'),
                            'data-method'  => 'post',

                        ]);
                    }


                ],
            ],
        ],
    ]); ?>


</div>
