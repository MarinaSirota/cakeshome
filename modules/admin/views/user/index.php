<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary'=>'Показано {begin}-{end} из {totalCount} ',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label' => 'Роль',
                'attribute'=>'user_type',
                'value'=>function($model){
                    if($model->user_type==1)
                        return 'Пользователь';
                    if($model->user_type==2)
                        return 'Кондитер';
                    if($model->user_type==3)
                        return 'Администатор';
                }
            ],
            'email:email',
            [
                'label' => 'Статус',
                'attribute'=>'active',
                'value'=>function($model){
                    if($model->active==1)
                        return 'Активен';
                    if($model->active==0)
                        return 'Не активен';
                }
            ],
            [
                'label' => 'Статус',
                'attribute'=>'block',
                'value'=>function($model){
//                   foreach($model->getModels as $user){
                    if ($model->block==1)
                         return 'Заблокирован';
                    if ($model->block==0)
                        return 'Не заблокирован';

                    }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Действия',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => '{view}{update}{delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'Посмотреть'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Редактировать'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Удалить'),
                            'data-confirm' => Yii::t('yii', 'Подтвердите удаление'),
                            'data-method'  => 'post',

                        ]);
                    }


                ],

            ],

        ],

    ]); ?>


</div>
