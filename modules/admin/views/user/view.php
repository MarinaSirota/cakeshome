<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <h1>Пользователь:<?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a($model->block==1? 'Разблокировать': 'Заблокировать', ['block', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
    </p>

    <?php if($model->user_type==2):?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'second_name',
            'third_name',
            'sity',
            'address',
            'description',
            'phone',
            'instagram',
            'site',
            'inn_number',
            [
                'label' => 'Роль',
                'attribute'=>'user_type',
                'value'=>function($model){
                    if($model->user_type==1)
                        return 'Пользователь';
                    if($model->user_type==2)
                        return 'Кондитер';
                    if($model->user_type==3)
                        return 'Администатор';
                }
            ],
            'email:email',
            [
                'label' => 'Статус',
                'attribute'=>'active',
                'value'=>function($model){
                    if($model->active==1)
                        return 'Активен';
                    if($model->active==0)
                        return 'Не активен';
                }
            ],

            [
                'label' => 'Статус',
                'attribute'=>'block',
                'value'=>function($model){
//                   foreach($model->getModels as $user){
                    if ($model->block==1)
                        return 'Заблокирован';
                    if ($model->block==0)
                        return 'Не заблокирован';

                }
            ],
        ],
    ]) ?>
    <?php endif;?>
    <?php if($model->user_type!=2):?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'second_name',
            'third_name',
            [
                'label' => 'Роль',
                'attribute'=>'user_type',
                'value'=>function($model){
                    if($model->user_type==1)
                        return 'Пользователь';
                    if($model->user_type==2)
                        return 'Кондитер';
                    if($model->user_type==3)
                        return 'Администатор';
                }
            ],
            'email:email',
            [
                'label' => 'Статус',
                'attribute'=>'active',
                'value'=>function($model){
                    if($model->active==1)
                        return 'Активен';
                    if($model->active==0)
                        return 'Не активен';
                }
            ],
            [
                'label' => 'Статус',
                'attribute'=>'block',
                'value'=>function($model){
//                   foreach($model->getModels as $user){
                    if ($model->block==1)
                        return 'Заблокирован';
                    if ($model->block==0)
                        return 'Не заблокирован';

                }
            ],
        ],
    ]) ?>
    <?php endif;?>


</div>
