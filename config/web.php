<?php

$params = require __DIR__ . '/params.php';
if(file_exists( __DIR__ . '/local/db.php')) {
    $db = require __DIR__ . '/local/db.php';
} else {
    $db = require __DIR__ . '/db.php';
}

$config = [
    'id' => 'basic',
    'name' => 'Cakes Home',
    'language' =>'ru',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'mEZupXDooyAbSGw_i_zf_VIpb8CRMxak',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport'=>[
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.mailgun.org',
                'username' => 'postmaster@mg.cakeshome.ru',
                'password' => 'af78c64f8d71c55f2e33d2b719a89dbc-7fba8a4e-b329c01a',
                'port' => '465',
                'encryption' => 'ssl',
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'catalog/category/<id:\d+>' => 'catalog/category',
                'catalog/product/<id:\d+>' => 'catalog/product',
                //'seller/user/<id:\d+>' => 'seller/user',
                'seller/cooker/<id:\d+>' => 'seller/cooker',
                'user/profile/<id:\d+>' => 'user/profile',

            ],
        ],

    ],
    'params' => $params,
    'modules'=> [
       'admin' =>'app\modules\admin\AdminModule',
        'cooker' =>'app\modules\cooker\CookerModule',
    ],
    'on beforeRequest' => function () {
        if(!\Yii::$app->user->isGuest && \Yii::$app->user->identity->active !== 1) {
            if(Yii::$app->request->url != '/site/logout') {
                Yii::$app->catchAll = [
                    'site/code',
                    //'message' => 'сайт не доступен'
                ];
            }
        }
    },
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
